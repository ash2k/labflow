run_container = action(
    attrs = {
        "image": attr.string(),
        "args": attr.list(
            item = attr.any_of(attr.string(), attr.future_getter()),
        ),
        "stdin": attr.future_getter(),
        "stdout": attr.future_setter(),
        "stderr": attr.future_setter(),
        "env_vars": attr.dict(
            key = attr.string(
                min_len = 1,
            ),
            value = attr.any_of(attr.string(), attr.future_getter()),
        ),
    },
)
