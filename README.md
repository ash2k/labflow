# LabFlow

LabFlow is a proof of concept (PoC) of an idea to allow the user to construct a CI pipeline (a graph of jobs) using
a program. It started as an attempt to build a [Petri net](https://en.wikipedia.org/wiki/Petri_net) generator for the
[Automatable DevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/330084) idea, but at the moment it can only
generate a directed acyclic graph (DAG). DAG generation is a simpler problem to solve - good for the
first iteration. Please see the Automatable DevOps issue for all the background information.

## Problems to solve

Issues that we have with CI and CI YAML are very well described in the
[CI/CD Template BluePrint MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/59833)
([direct link to doc](https://gitlab.com/gitlab-org/gitlab/-/blob/736d610475d9c99a436a3d3ead0ce114a1a5a6fd/doc/architecture/blueprints/ci_template/index.md#problems)).
I was not aware of this effort when I started building LabFlow, and the purpose was not to solve those problems
(see above). However, at this stage I think it might be beneficial to share this PoC.

In addition to the above - there is no formalized way to pass data between jobs. Interacting jobs have to
share a convention on what environment variables to respect and what files/artifacts to write/read.
Put another way - there is no API-like layer to decouple producer and consumer(s), and to verify the interaction.
Conventions don't work well in this situation because it's impossible to check the correctness of the
specification beyond just syntax without actually running the build. Ideally it should be possible to verify the graph
of jobs without executing it - running even a 10-minute build to discover a typo in a file path is not the best UX.

## Demo

<https://youtu.be/FqtoZMH8zS0>

## How LabFlow works

LabFlow interprets the user's program. That program uses provided mechanisms, described below, to
construct a graph of actions (think graph of CI jobs). That graph can then be fed into GitLab Runner as a CI pipeline.
This last step is not implemented - LabFlow has features ([providers](#provider) and [futures](#future)) that don't
directly map into the current CI YAML.

The idea is that LabFlow evaluator can be run by:

- GitLab Runner - either as an independent (or even hidden) job or just be
  part of Runner itself (or maybe a separate binary that is shipped along with it). Once the graph is built,
  it's sent to GitLab, which turns it into a pipeline, which is fed back into Runner for execution.
- Pipeline developer. Anyone should be able to run a LabFlow program locally to check that it produces
  an expected output (graph of CI jobs).

Next sections describe how things work starting from the lower-level building primitives and going
up the stack from there i.e. later sections refer to former sections.

### Illustrations legend

There are some illustrations below, here is the legend for them:

- Arrows mean "depends on". I.e. `A -> B` means `A` depends on `B`.
- Blue ellipses are actions.
- Green diamonds are futures.
- Yellow boxes are steps.
- Graph should be read in the bottom -> top direction i.e. execution starts at the bottom of the
  image and goes up to the top. It's just how Graphviz draws it.
- Arrows can be one of 3 colors:
  - Green for `future -> action` and `action -> future` dependencies.
  - Brown for explicit `action -> action` dependencies.
  - Blue for `action -> action` dependencies that were created by the use of the `deps` attribute.

### `load()`

[`load()`](https://github.com/google/starlark-go/blob/master/doc/spec.md#load-statements) is a Starlark builtin statement
that allows to load certain symbols from a module into the current module. It's a way to import a library, both local or
potentially remote. Fetching a remote module should be implemented as a separate mechanism, but to load
symbols from it one would still use the `load()` statement.

`load()` allows to re-use code, both within a single repository and across repositories. Both GitLab and users can
build and share bits and pieces for LabFlow. This solves the problem of YAML template re-use.

Example: [example/basic/lib_job.star](example/basic/lib_job.star) defines `job` and
[example/basic/ci.star](example/basic/ci.star) uses it.

###  `schema()`

`schema()` is a function that allows to define schemas. Schemas are used to validate values. Starlark is a dynamically
typed language and hence there is no builtin type checking or argument validation. Schemas are a way to compensate
for the lack of both by allowing for powerful runtime value validation.
Another way to put it is - `schema()` allows to turn imperative validation logic into a declarative building
block for argument validation. Such declarative blocks can be composed into arbitrarily complex validators.

`schema()` function takes a constructor function as a sole argument. Its job is to:

1. Validate the passed arguments. `min_len` in the example below; only one argument in this case. These arguments
   are a way to configure the validating function.
1. Return the validating function (`check()`). It must take a single argument, the value to validate, and must return
   the validated and defaulted (in the future) value. The returned value doesn't have to be but is often of the
   same type.

When invoked, `schema()` returns a value of `schema_ctor` type i.e. a constructor of a `schema` value.

```python
def schema_string_ctor(min_len=0):  # constructor
    if type(min_len) != "int":
        fail("min_len: expecting int, got: %s" % type(min_len))

    def check(val):  # validator. Can use variables from the outer scope e.g. min_len
        if type(val) != "string":
            fail("value %s is not a string" % val)
        if len(val) < min_len:
            fail("string '%s' is shorter than min length %d" % (val, min_len))
        return val

    return check


schema_string = schema(schema_string_ctor)

is_good_string = schema_string(min_len=2)  # schema_string_ctor() is called here to construct an instance of the schema
is_a_string = schema_string()  # min_len has a default value so can be omitted

# Usage. check() is called each time to perform validation.
is_good_string(1)  # fails with an error
is_good_string("1")  # fails with an error
validated = is_good_string("12")  # succeeds
```

Composition example ([`attr`](#attr) is described in the next section):

```python
list_of_dicts_of_good_strings = attr.list(
    item=attr.dict(
        key=schema_string(min_len=5),
        value=is_good_string,
    ),
)

# Usage
validated = list_of_dicts_of_good_strings([])  # succeeds
validated = list_of_dicts_of_good_strings([{"abcdefg": "12"}])  # succeeds
list_of_dicts_of_good_strings([123])  # fails with an error
```

Schemas are used by [providers](#provider), [steps](#steps) and [futures](#future) to validate attributes.

### `attr`

`attr` is a holder of predeclared schemas. You can see what's currently available and how it works in
[predeclared1_schemas.star](predeclared1_schemas.star). They can certainly be improved,
but are a good start to show the idea and power of schemas. More schemas can be defined by adding code to this file.

### Actions

A LabFlow action is a definitions of a task to be performed by an executor (Runner or GitLab). The simplest
example is a CI job for Runner - in LabFlow it's modelled as the `run_container()` action.
However, actions are more generic (if we need that), they are not limited to just running containers.

Examples of potential actions:

- Low-level and generic: make an HTTP call.
- Middle-level: call a certain GitLab API.
- High-level: manipulate a DevOps domain entity e.g. create a release, deploy a release,
  create/close an issue, open an MR, etc.

### `provider()`

`provider()` is a function that allows to create a "provider constructor", also referred to "provider type".
"Provider constructor" contains schemas of attributes of a "provider" and can be used to create one. An example:

```python
# Message is a provider constructor. Like "type Message struct { message string }" in Go.
Message = provider(
    attrs = {
        message: attr.string(),
    },
)

# Like "msg1 := Message{ message: "Hello, world!" }" in Go.
msg1 = Message(message="Hello, world!") # msg1 is a Message provider.
msg2 = Message(message="Hello, GitLab!") # msg2 is a Message provider
print(msg1.message)
```

Providers are a way to pass structured data around. See the [Steps](#steps) section on learn
how providers are used.

### Steps

A step is a group of one or more actions. You can think of a step as of a reusable piece of CI YAML
that defines one or more CI jobs.

Library authors use steps to wrap action(s) into higher-level building blocks,
suitable for external consumption. [example/complex/lib_scan_image.star](example/complex/lib_scan_image.star) is an
example of that - a GitLab team could provide this as a library to use in users' builds to scan published container
images.

Users use the steps to define their own groups of actions.
[example/complex/ci.star](example/complex/ci.star) is an example of that - the container scanning library
mentioned above is consumed here in a user-defined step.

Steps can depend on other steps:
- To consume information they will produce - via [providers](#provider).
- For ordering purposes - when a step depends on side effects of another step(s).

#### Label

"Label" is a string that contains an address of a step. At the moment this is limited to just `:<step name>` but
can grow to something more sophisticated if needed (like in Bazel).

#### `step()`

`step()` is a function that allows to define step constructors. It takes two arguments:

- `implementation` - an implementation function.
- `attrs` - a dictionary of schemas of attributes for step creation.

#### Implementation function

An implementation function takes a single argument `ctx`. It can be used to:

- Access step's actual arguments. They are available via `ctx.attrs.<attribute name>`.
- Instantiate actions. Predeclared actions are available via `ctx.actions.<action name>(<action arguments>)`.

Actions are predeclared in [predeclared3_actions.star](predeclared3_actions.star). More actions can be defined by
adding code to this file. The `action()` function that is used there is only available in that file. It takes a
dictionary of attributes the action takes as a sole argument. It's a good example of how
schemas can be used to declaratively define validation rules for arguments.

Example:

```python
# job_impl is a simple implementation function
def job_impl(ctx):
    ctx.actions.run_container(
        image=ctx.attrs.image,
        args=getattr(ctx.attrs, "args", []),
    )


# job is a step constructor
job = step(
    implementation=job_impl,
    attrs={
        "image": attr.string(),
        "args": attr.list(
            item=attr.string(),
        ),
    },
)

# job() invocation instantiates a step named "job1".
# job_impl() is called here and it instantiates a run_container() action.
job(
    name="job1",
    image="img1:v3",
    # args is omitted here and it will be set to an empty list in the implementation function
)
```

LabFlow visualizes the above program like this:

![trivial1.star visualization](example/trivial/trivial1.svg "Example visualization")

#### Providers

Providers are pieces of information that a step exposes to other steps that depend on it.
This data can be anything a step’s consumers should know about. See [`provider()`](#provider).

Since a step’s implementation function can only read providers from the instantiated step’s immediate dependencies,
step implementation functions need to forward any information from their dependencies that needs to be known by
it's consumers.

A step’s providers are specified by a list of `provider` objects returned by the implementation function.
Providers must be unique in that list i.e. at most one provider constructed by a particular provider constructor
can be returned.

When an implementation function is invoked, all `label` values, including the arbitrarily nested ones
(e.g. a list of labels), are turned into a dictionary of `provider type -> provider` mappings. That way consumer
can retrieve a provider by its type/constructor.

As you can see below, the `to_print` attribute has a type of `label`. The implementation function retrieves the
`Message` provider by it's type to get the message to print.

The program below is not very useful as it constructs an empty graph. However, it shows how information can be exposed
by a step and consumed by another step. This program will print "Hello, Sid!" when evaluated. See the [futures](#future)
section below for an evolution of this example.

```python
Message = provider(
    attrs={
        "message": attr.string(),
    },
)


def produce_greeting_impl(ctx):
    return [
        Message(message="Hello, %s!" % ctx.attrs.person),
        # returning more than one Message here would cause an error
    ]


produce_greeting = step(
    implementation=produce_greeting_impl,
    attrs={
        "person": attr.string(),
    },
)

produce_greeting(
    name="greeting1",
    person="Sid",
)


def print_message_impl(ctx):
    # print() is a Starlark builtin function that prints to stderr. Used for debugging/logging.
    print(ctx.attrs.to_print[Message].message)


print_message = step(
    implementation=print_message_impl,
    attrs={
        "to_print": attr.label(
            providers=[  # list of must-be-supplied-by-dependency provider types
                Message,
            ],
        ),
    },
)

print_message(
    name="print1",
    to_print=":greeting1",
)
```

#### `future()`

Futures are a mechanism to produce and consume data in the future. Coupled with providers, it's
a facility to pass data from an action in a producing step to zero or more actions in a consuming step(s).

A future is actually two objects:
- Future setter.
- Future getter.

A future can be written to by:
- Piping into future setter from stderr or stdout.
- Getting the file name of the future setter, passing it to an action, and writing to it.

A future can be read from by:
- Piping future getter into stdin, an environment variable or a command line argument.
- Getting the file name of the future getter, passing it to an action, and reading from it.

Future setters can be called:
- Without arguments. This returns the name of the file for the producer.
- With a sole argument which must be of the "action" type. This returns the future getter.

Let's rewrite the above example to make it generate and print a greeting using actions rather
than during graph construction.

```python
Message = provider(
    attrs={
        "message": attr.future_getter(),  # future getter for reading.
    },
)


def produce_greeting_impl(ctx):
    msg_set = ctx.future()  # instantiate a new future setter.
    msg_action = ctx.actions.run_container(
        image="greeter:v1",
        args=[
            "--output", msg_set(),  # where to write the greeting
            "--person", ctx.attrs.person,  # Who to greet
        ],
    )
    msg_get = msg_set(msg_action)  # future getter
    return [
        Message(message=msg_get),
    ]


produce_greeting = step(
    implementation=produce_greeting_impl,
    attrs={
        "person": attr.string(),
    },
)

produce_greeting(
    name="greeting1",
    person="Sid",
)


def print_message_impl(ctx):
    message = ctx.attrs.to_print[Message].message
    ctx.actions.run_container(
        image="printer:v1",
        args=[
            # Where to read the greeting from.
            # Note how message is used as a function to get the file name.
            "--input", message(),
        ],
        futures=[message],  # declare dependency from this action onto the producer action via the future.
    )
    # An alternative implementation where contents of the input file are piped into stdin.
    # Note how message is used as a value rather than a function.
    # ctx.actions.run_container(
    #     image="printer:v1",
    #     stdin=message,
    # )


print_message = step(
    implementation=print_message_impl,
    attrs={
        "to_print": attr.label(
            providers=[  # list of must-be-supplied-by-dependency provider types
                Message,
            ],
        ),
    },
)

print_message(
    name="print1",
    to_print=":greeting1",
)
```

LabFlow visualizes the above program like this:

![hello2.star visualization](example/hello/hello2.svg "Example visualization")

[example/complex/lib_scan_image.star](example/complex/lib_scan_image.star) is an example where futures are used to
pass the published container image name and digest (i.e. `image@sha256:...`) to the container scanning step.
- Why not pre-construct and pass the `image:<tag>` e.g. `image:latest` to both actions?
  Because multiple concurrent builds may be pushing to the same tag and then it's not clear which one
  was scanned and which one was not. Something might sneak past the security scanner and we don't want that.
- Why not tag each image in a unique way e.g. `image:<build-id>` and pass that to both actions?
  Because then container storage costs would keep growing as registry's garbage collector would not
  consider tagged layers unused and hence would not delete them.

Another example is any [MapReduce](https://en.wikipedia.org/wiki/MapReduce)-style pipeline - N jobs process
a chunk of data each (map phase) and publish futures with the result, then another job
aggregates (reduce phase) the results into a single result. E.g. concurrently building subprojects of a
huge project and then publishing a single archive with all the artifacts. This is possible today but the
aggregator (the reduce phase) has to rely on conventionally placed files and there is no way to verify outputs will be
where they are expected without running the build.

### `DefaultInfo` provider

`DefaultInfo` is a predeclared provider that describes actions of a step (more fields may be added in the
future if needed). By default, LabFlow would add an instance of
`DefaultInfo` to the list of returned providers with all the actions a step instantiated.
A step may return an instance of `DefaultInfo` to override the default behavior.

`DefaultInfo` is used by LabFlow to create dependencies between steps via the `deps` attribute. A step implementation
can also use it for some other purpose.

#### Predefined attributes

All steps get the following predefined attributes:

- `name` is the name of a step. It can be used to construct a label to address this step.
- `deps` is a list of labels i.e. a list of steps this step depends on. Each action a step instantiates depends
  on all actions listed in `DefaultInfo` of all steps, listed in the `deps` attribute.
  It's a way to create ordering of actions that don't consume/produce data but only side effects.

Below `job1` is the name of the first `job()` invocation.
The second one addresses the first one using `:job1`, which is a label.

```python
job(
    name="job1",  # <-- name
    image="img1:v3",
)

job(
    name="job2",
    image="img1:v3",
    deps=[
        ":job1",  # <-- label
    ],
)
```

LabFlow visualizes the above program like this:

![trivial2.star visualization](example/trivial/trivial2.svg "Example visualization")

#### Backward/forward compatibility via providers

In producer-defined providers, if a producer needs to make a breaking change to a provider,
they just define a new provider type and return an instance of it in addition to the current version.
Then consumers migrate to the new version, and the old one can be dropped.

In consumer-defined providers, if the consumer needs to make a breaking change to a provider,
they just define a new provider type and dynamically check which version the producer is providing. Once all producers
have migrated to the new version, and the old one can be dropped.

### Macro

A macro is a function that calls one or more step functions. It's a way to adjust parameters to and/or group steps.

```python
def push(name, both=False):
    build_and_publish_image(
        name=name + "1",
        image="img1",
    )
    if both:
        build_and_publish_image(
            name=name + "2",
            image="img2",
            deps=[
                ":push_image1",
            ],
        )


push("push_image", True)
```

### Predeclared symbols

In addition to the LabFlow-specific symbols described above, there are Starlark-standard symbols. Please see the
[language specification](https://github.com/google/starlark-go/blob/master/doc/spec.md) for the full list.

Various additional libraries are available if we need them:

- https://github.com/google/starlark-go/tree/master/lib
- https://github.com/qri-io/starlib

It's trivial to build libraries for Go implementation of Starlark:
- A library can be written in Starlark itself.
- A library can be loaded as a builtin into the LabFlow program by the LabFlow evaluator (this project).
  Builtins are a way to delegate to Go code - any Go library can be used that way. Care must be taken to ensure it's
  deterministic to avoid subverting the benefits of Starlark.

## Relation to [Bazel](https://bazel.build/)

LabFlow is heavily inspired by Bazel and Bazel users would immediately recognize the patterns.

| **LabFlow**   | **Bazel**                                                                   | Notes |
| ------------- | --------------------------------------------------------------------------- | ----- |
| `step()`      | [`rule()`](https://docs.bazel.build/versions/main/skylark/rules.html)       | |
| action        | [action](https://docs.bazel.build/versions/main/skylark/rules.html#actions) | |
| macro         | [macro](https://docs.bazel.build/versions/main/skylark/macros.html)         | |
| label         | [label](https://docs.bazel.build/versions/main/build-ref.html#labels)       | |
| `load()`      | [`load()`](https://docs.bazel.build/versions/main/build-ref.html#load)      | |
| `attr`        | [`attr`](https://docs.bazel.build/versions/main/skylark/lib/attr.html)      | |
| `provider()`  | [`provider()`](https://docs.bazel.build/versions/main/skylark/lib/Provider.html) | |
| `schema()`    | - | Bazel has a fixed set of schemas in `attr` and there is no way to extend it. In LabFlow it is possible with `schema()`. |
| `future()`    | [`File`](https://docs.bazel.build/versions/main/skylark/lib/File.html) | `File` serves the same purpose but works differently. |
| `DefaultInfo` | [`DefaultInfo`](https://docs.bazel.build/versions/main/skylark/lib/DefaultInfo.html) | |

## Why Starlark

As a CI user, as long as I haven't made any changes to the build definition, I expect the graph of jobs in it to
be **identical** from execution to execution.

CI jobs graph generator must be [deterministic](https://en.wikipedia.org/wiki/Deterministic_algorithm) to satisfy the
above requirement.

From https://github.com/google/starlark-go:

> Starlark is a dialect of Python intended for use as a configuration language. Like Python, it is an untyped dynamic language with high-level data types, first-class functions with lexical scope, and garbage collection. Starlark is a small and simple language with a familiar and highly readable syntax. You can use it as an expressive notation for structured data, defining functions to eliminate repetition, or you can use it to add scripting capabilities to an existing application.
>
> A Starlark interpreter is typically embedded within a larger application, and the application may define additional domain-specific functions and data types beyond those provided by the core language. For example, Starlark was originally developed for the Bazel build tool. Bazel uses Starlark as the notation both for its BUILD files (like Makefiles, these declare the executables, libraries, and tests in a directory) and for its macro language, through which Bazel is extended with custom logic to support new languages and compilers.

From https://github.com/google/starlark-go/blob/master/doc/spec.md:

> Starlark is intended not for writing applications but for expressing configuration: its programs are short-lived and have no external side effects and their main result is structured data or side effects on the host application. As a result, Starlark has no need for classes, exceptions, reflection, concurrency, and other such features of Python.
>
> Starlark execution is **deterministic**: all functions and operators in the core language produce the same execution each time the program is run; there are no sources of random numbers, clocks, or unspecified iterators. This makes Starlark suitable for use in applications where reproducibility is paramount, such as build tools.

Given the above, Starlark is an excellent language to write deterministic programs as it has been built
for exactly that.

Also see [Declarative vs imperative](https://gitlab.com/gitlab-org/gitlab/-/issues/330084#declarative-vs-imperative)
for the background on the approach taken.

## New ideas and workflows LabFlow enables

- GitLab can recognize certain provider types and handle them automatically without any explicit code in the LabFlow
  program. There are [17 report types](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsreports) and it's
  probably a sign that there are too many special cases that require special syntax and some generalization is needed.
  Perhaps it could just be a `JUnitReport` provider for a JUnit report and so on for each case.
- Some concepts that are present in the CI YAML file today may become unnecessary or are simplified:
  - [`parallel`](https://docs.gitlab.com/ee/ci/yaml/README.html#parallel) becomes a `for` loop
    to construct multiple parallel steps. They are parallel because they don't depend on each other:
    ```python
    for i in range(10): # 10 copies
        my_custom_job(
            name = "instance_%d" % i,
            # args...
        )
    ```
  - [`matrix`](https://docs.gitlab.com/ee/ci/yaml/README.html#parallel-matrix-jobs)
    becomes a `for` loop too (or a list comprehension):
    ```python
    ARCH = ["x86", "x64"]
    OS = ["linux", "macOS"]
    for os in OS:
        for arch in ARCH:
            if os == "macOS" and arch != "x64": # only want 64 bit macOS builds
                continue
            my_custom_job(
                name = "instance_%s_%s" % (os, arch),
                os = os,
                arch = arch,
            )
    ```
  - ...
- We could generate documentation for a LabFlow library from its sources as we understand the structure - attributes
  and their types, available steps and macros. We could have a convention to not consider
  symbols starting with an underscore exported and allow authors to hide implementation details.
  No docs for such symbols.
- LabFlow evaluator should be runnable locally to allow rapid iteration on the graph.
- LabFlow code should be easy to test - for a defined input processing should generate an expected output. We could
  devise a way to record the graph in a text format and use that as the format for the expected output.
- ...

## Try it yourself

LabFlow outputs the graph:
- To stderr in plain text as vertices and edges.
- To stdout in the [DOT format](https://www.graphviz.org/doc/info/lang.html).

1. To run LabFlow you'll need to have [Go installed](https://golang.org/dl/). For macOS:
   ```shell
   brew install go
   ```
1. (optional) To visualize the graph you'll need to have [Graphviz installed](https://www.graphviz.org/download/).
   For macOS:
   ```shell
   brew install graphviz
   ```
1. Clone this repository:
   ```shell
   git clone https://gitlab.com/ash2k/labflow.git
   cd labflow
   ```
1. Use LabFlow to evaluate one of the example scripts:
   ```shell
   go run ./cmd ./example/basic/ci.star | dot -Tsvg > ./basic_ci.svg
   # should open the file in e.g. your browser
   open ./basic_ci.svg

   go run ./cmd ./example/complex/ci.star | dot -Tsvg > ./complex_ci.svg
   open ./complex_ci.svg
   ```

Try changing something and see the result. The simplest things to try are:
- Supply an invalid value.
- Make a syntax error.
- Introduce a new dependency and see the graph change.

## Next steps

If we want to pursue the Automatable DevOps idea, this project needs to be reworked to generate Petri nets rather than DAGs.
In its current form it can be used to solve some (most?) of the GitLab CI YAML challenges, but that was not
the original goal.

### Missing functionality

- Loading and consuming  (via `load()`) external modules. This is needed so that users can publish and consume
  libraries. It must be possible to refer to certain immutable versions/snapshots of libraries to make the build
  reproducible. This can be solved the same way Bazel solves it:
  - Git repository checkout on a particular commit.
    Bazel's [`git_repository()`](https://docs.bazel.build/versions/main/repo/git.html#git_repository)
  - Downloading an archive from a URL and validating the hash of it to ensure it's the same thing.
    Bazel's [`http_archive()`](https://docs.bazel.build/versions/main/repo/http.html#http_archive).
- `schema()` should support supplying default values.
- `schema()` should support allowing optional values.
- A way to parametrize the whole program. E.g. how to generate a different graph based on the name of the branch.

  Potential solutions:
  - Pass global parameters via a predeclared global symbol.
  - Wrap the whole program into a function that accepts a parameter - an analog of `main(ctx)` - where `ctx` holds
    the global parameters. Then LabFlow would invoke that function.
- How to make it possible to evaluate a parameterized (see the previous item) LabFlow program and render the graph?

  Potential solutions:
  - Explicitly declare required parameters (e.g. branch name) that the program consumes and make it possible to
    specify them when viewing the graph via a UI so that a user could see how the graph looks for certain parameters.
    Same for development loop/local evaluation/tests.
  - In addition to the above, make it possible to specify default values for required parameters and use
    them when rendering the graph.

- ...

### Open questions and things to consider

- [Diamond dependencies](https://en.wikipedia.org/wiki/Dependency_hell) between libraries.
- Is LabFlow's value/complexity ratio high enough? How to improve it?
- ...
