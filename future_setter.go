package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
)

const (
	futureSetterType = "future_setter"
	futureSetterName = "future_setter"
)

var (
	_ starlark.Callable = (*futureSetter)(nil)
)

type futureSetter struct {
	setFile        string
	getFile        string
	validateAction func(*actionInstance) bool
	get            *futureGetter
	frozen         bool
}

func (s *futureSetter) String() string {
	return fmt.Sprintf("<%s: %s>", s.Name(), s.setFile)
}

func (s *futureSetter) Type() string {
	return futureSetterType
}

func (s *futureSetter) Truth() starlark.Bool {
	return s.get != nil
}

func (s *futureSetter) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", s.Type())
}

func (s *futureSetter) Name() string {
	return futureSetterName
}

// CallInternal returns the name of the setter file if called without arguments or sets the future value if
// it's called with a single positional argument. Everything else is an error.
func (s *futureSetter) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if args.Len() == 0 && len(kwargs) == 0 {
		return s.getCallInternal()
	}
	return s.setCallInternal(args, kwargs)
}

func (s *futureSetter) getCallInternal() (starlark.Value, error) {
	return starlark.String(s.setFile), nil
}

func (s *futureSetter) setCallInternal(args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if s.frozen {
		return nil, fmt.Errorf("%s: cannot set frozen future", s.Name())
	}
	if s.get != nil {
		return nil, fmt.Errorf("%s: value of the future has already been set", s.Name())
	}
	var value starlark.Value
	err := starlark.UnpackPositionalArgs(s.Name(), args, kwargs, 1, &value)
	if err != nil {
		return nil, err
	}
	switch value.(type) {
	case starlark.String, *actionInstance:
		if a, ok := value.(*actionInstance); ok {
			if !s.validateAction(a) {
				return nil, fmt.Errorf("%s: future must used an action, belonging to the same step", s.Name())
			}
		}
		s.get = &futureGetter{
			file:  s.getFile,
			set:   s,
			value: value,
		}
		s.get.Freeze()
		return s.get, nil
	default:
		return nil, fmt.Errorf("%s: expecting an action or a string, got: %s", s.Name(), value.Type())
	}
}

func (s *futureSetter) Freeze() {
	s.frozen = true
}
