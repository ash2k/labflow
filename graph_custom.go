package labflow

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/simple"
)

var (
	_ graph.Directed           = (*customGraph)(nil)
	_ dot.Graph                = (*customGraph)(nil)
	_ dot.AttributeSetters     = (*customGraph)(nil)
	_ dot.Attributers          = (*customGraph)(nil)
	_ dot.Structurer           = (*customGraph)(nil)
	_ encoding.AttributeSetter = (attributes)(nil)
	_ encoding.Attributer      = (attributes)(nil)
	_ graph.Edge               = (*customEdge)(nil)
	_ encoding.AttributeSetter = (*customEdge)(nil)
	_ encoding.Attributer      = (*customEdge)(nil)
)

type customGraph struct {
	*simple.DirectedGraph
	graph, node, edge attributes
	sub               []dot.Graph
	name              string
}

func newCustomGraph(name string) *customGraph {
	return &customGraph{
		DirectedGraph: simple.NewDirectedGraph(),
		graph:         attributes{},
		node:          attributes{},
		edge:          attributes{},
		name:          name,
	}
}

// NewEdge returns a new Edge from the source to the destination node.
func (g *customGraph) NewEdge(from, to graph.Node) graph.Edge {
	return &customEdge{
		Edge:       g.DirectedGraph.NewEdge(from, to),
		attributes: attributes{},
	}
}

func (g *customGraph) Structure() []dot.Graph {
	return g.sub
}

func (g *customGraph) DOTID() string {
	return g.name
}

// DOTAttributers implements the dot.Attributers interface.
func (g *customGraph) DOTAttributers() (graph, node, edge encoding.Attributer) {
	return g.graph, g.node, g.edge
}

// DOTAttributeSetters implements the dot.AttributeSetters interface.
func (g *customGraph) DOTAttributeSetters() (graph, node, edge encoding.AttributeSetter) {
	return &g.graph, &g.node, &g.edge
}

type attributes map[string]string

func (a attributes) Attributes() []encoding.Attribute {
	res := make([]encoding.Attribute, 0, len(a))
	for k, v := range a {
		res = append(res, encoding.Attribute{
			Key:   k,
			Value: v,
		})
	}
	return res
}

func (a attributes) SetAttribute(attr encoding.Attribute) error {
	a[attr.Key] = attr.Value
	return nil
}

// customEdge extends simple.Edge to support DOT edge attributes.
type customEdge struct {
	graph.Edge
	attributes
}
