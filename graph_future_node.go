package labflow

import (
	"strconv"

	"go.starlark.net/starlark"
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
)

var (
	_ graph.Node               = (*FutureNode)(nil)
	_ encoding.AttributeSetter = (*FutureNode)(nil)
	_ encoding.Attributer      = (*FutureNode)(nil)
)

type FutureNode struct {
	graph.Node
	future *futureSetter
	attrs  map[string]string
}

func (n *FutureNode) Future() starlark.Value {
	return n.future
}

// Attributes returns the DOT attributes of the node.
func (n *FutureNode) Attributes() []encoding.Attribute {
	res := make([]encoding.Attribute, 0, len(n.attrs)+1)
	if _, ok := n.attrs["label"]; !ok {
		res = append(res, encoding.Attribute{
			Key:   "label",
			Value: strconv.Quote(n.future.String()),
		})
	}
	for k, v := range n.attrs {
		res = append(res, encoding.Attribute{
			Key:   k,
			Value: v,
		})
	}
	return res
}

// SetAttribute sets a DOT attribute.
func (n *FutureNode) SetAttribute(attr encoding.Attribute) error {
	n.attrs[attr.Key] = attr.Value
	return nil
}
