package labflow

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.starlark.net/starlark"
)

func TestParseLabel_Valid(t *testing.T) {
	tests := []struct {
		lbl    starlark.String
		parsed label
	}{
		{
			lbl: ":abc",
			parsed: label{
				Name: "abc",
			},
		},
	}
	for _, tc := range tests {
		t.Run(string(tc.lbl), func(t *testing.T) {
			parsed, err := parseLabel(tc.lbl)
			require.NoError(t, err)
			assert.Empty(t, cmp.Diff(tc.parsed, parsed))
		})
	}
}

func TestParseLabel_Invalid(t *testing.T) {
	tests := []starlark.String{
		"abc",
		":колобок",
	}
	for _, tc := range tests {
		t.Run(string(tc), func(t *testing.T) {
			_, err := parseLabel(tc)
			require.Error(t, err)
		})
	}
}
