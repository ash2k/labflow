package labflow

import (
	"fmt"
	"path"

	"go.starlark.net/starlark"
)

func (c *ctx) newCtxFuture(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	fname := fmt.Sprintf("future_%d", len(c.futures))
	// Using "path" and not "filepath" to ensure deterministic unix-style names are generated even on other OSes.
	set := &futureSetter{
		setFile:        path.Join(c.futurePath, "set", c.stepName.Name, fname),
		getFile:        path.Join(c.futurePath, "get", c.stepName.Name, fname),
		validateAction: c.validateAction,
	}
	c.futures = append(c.futures, set)
	return set, nil
}
