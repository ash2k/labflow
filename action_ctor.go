package labflow

import (
	"errors"
	"fmt"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"go.starlark.net/syntax"
)

const (
	actionCtorType = "action_ctor"
	actionCtorName = "action_ctor"
)

type actionCtor struct {
	starlarkstruct.Struct
	attrSchemas map[string]schema // don't need to freeze because same values as in Struct
	name        string
}

func (c *actionCtor) String() string {
	return "<action_ctor>" // TODO
}

func (c *actionCtor) Type() string {
	return actionCtorType
}

func (c *actionCtor) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", c.Type())
}

func (c *actionCtor) Name() string {
	return c.name
}

func (c *actionCtor) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if args.Len() > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", c.Name())
	}
	kwargs, err := validateAndDefaultKwargs(thread, kwargs, c.attrSchemas)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", c.Name(), err)
	}
	action := &actionInstance{
		Struct: *starlarkstruct.FromKeywords(starlark.String(c.name), kwargs),
	}
	action.Freeze()
	return action, nil
}

func (c *actionCtor) CompareSameType(op syntax.Token, yy starlark.Value, depth int) (bool, error) {
	y := yy.(*actionCtor)
	switch op {
	case syntax.EQL:
		return c == y, nil
	case syntax.NEQ:
		return c != y, nil
	default:
		return false, fmt.Errorf("%s %s %s not implemented", c.Type(), op, yy.Type())
	}
}

func newActionCtor(reservedAttributes map[string]schema) func(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	return func(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
		if args.Len() > 0 {
			return nil, errors.New("provider_ctor: unexpected positional arguments")
		}
		var (
			attrs       *starlark.Dict
			attrsKwargs []starlark.Tuple
			attrSchemas map[string]schema
		)
		err := starlark.UnpackArgs("action", nil, kwargs, "attrs??", &attrs)
		if err != nil {
			return nil, err
		}
		if attrs != nil {
			attrsKwargs = attrs.Items()
			attrSchemas, err = kwargsToSchemas(attrsKwargs, reservedAttributes)
			if err != nil {
				return nil, fmt.Errorf("action: %w", err)
			}
		}
		ctor := &actionCtor{
			Struct:      *starlarkstruct.FromKeywords(starlark.String(actionCtorName), attrsKwargs),
			attrSchemas: attrSchemas,
			name:        "<unknown>",
		}
		ctor.Freeze()
		return ctor, nil
	}
}
