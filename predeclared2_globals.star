# must be named DefaultInfo
DefaultInfo = provider(
    attrs = {
        # must have `actions` attribute
        "actions": attr.list(
            item = attr.action(),
        ),
    },
)

# must be named step_attributes
step_attributes = {
    # must have `name` attribute
    "name": attr.string(
        min_len = 1,
    ),
    # must have `deps` attribute
    "deps": attr.list(
        item = attr.label(),
    ),
}

# must be named action_attributes
action_attributes = {
    # must have `deps` attribute
    "deps": attr.list(
        item = attr.action(),
    ),
    # must have `futures` attribute
    "futures": attr.list(
        item = attr.future_getter(),
    ),
}
