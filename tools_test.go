package labflow

import (
	"testing"
)

func TestPtrToHash64(t *testing.T) {
	var x uint64 = 1 << 33
	x++
	h := uint32(x>>32) ^ uint32(x)
	if h != 0b11 {
		t.Fatalf("%b", h)
	}
}
