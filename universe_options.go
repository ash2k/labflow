package labflow

import (
	"go.starlark.net/starlark"
)

type UniverseOption func(*universeConfig)

type universeConfig struct {
	// futurePath is the filesystem path for future's files.
	futurePath       string
	extraPredeclared starlark.StringDict
	programSrc       interface{}
}

func defaultUniverseConfig() universeConfig {
	return universeConfig{
		futurePath: "/f",
	}
}

func applyOptions(opts []UniverseOption) universeConfig {
	config := defaultUniverseConfig()
	for _, v := range opts {
		v(&config)
	}
	return config
}

func WithFuturePath(futurePath string) UniverseOption {
	return func(config *universeConfig) {
		config.futurePath = futurePath
	}
}

func WithExtraPredeclared(extraPredeclared starlark.StringDict) UniverseOption {
	return func(config *universeConfig) {
		config.extraPredeclared = extraPredeclared
	}
}

func WithProgramSrc(programSrc interface{}) UniverseOption {
	return func(config *universeConfig) {
		config.programSrc = programSrc
	}
}
