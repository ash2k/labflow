package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
)

const (
	schemaName = "schema"
	schemaType = "schema"
)

var (
	_ starlark.Callable = schema{}
)

type schema struct {
	check starlark.Callable
}

func (s schema) String() string {
	return fmt.Sprintf("<%s>", s.Name())
}

func (s schema) Type() string {
	return schemaType
}

func (s schema) Freeze() {
}

func (s schema) Truth() starlark.Bool {
	return true
}

func (s schema) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", s.Type())
}

func (s schema) Name() string {
	return schemaName
}

func (s schema) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var val starlark.Value
	err := starlark.UnpackPositionalArgs(s.Name(), args, kwargs, 1, &val)
	if err != nil {
		return nil, err
	}
	return starlark.Call(thread, s.check, args, nil)
}
