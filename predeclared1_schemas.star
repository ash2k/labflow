def assert_type(val, type_str):
    if type(val) != type_str:
        fail("expecting value of type '%s' but got '%s'; value is: %r" % (type_str, type(val), val))

def assert_none_or_type(val, type_str):
    if val != None:
        assert_type(val, type_str)

def assert_type_provider_ctor(val):
    assert_type(val, "provider_ctor")

def assert_type_schema(val):
    assert_type(val, "schema")

def assert_list_of(val, s):
    assert_type(val, "list")
    for v in val:
        s(v)

###############

def schema_as_is_ctor():
    def check(val):
        return val

    return check

as_is = schema(schema_as_is_ctor)()

###############

def schema_string_ctor(min_len = 0):
    assert_type(min_len, "int")

    def check(val):
        assert_type(val, "string")
        if len(val) < min_len:
            fail("string %r is shorter than minimum length %d" % (val, min_len))
        return val

    return check

schema_string = schema(schema_string_ctor)

###############

def schema_bool_ctor():
    def check(val):
        assert_type(val, "bool")
        return val

    return check

schema_bool = schema(schema_bool_ctor)

###############

def schema_int_ctor(min=None, max=None):
    assert_none_or_type(min, "int")
    assert_none_or_type(max, "int")

    def check(val):
        assert_type(val, "int")
        if min != None:
            if val < min:
                fail("expecting an integer greater or equal to %d, got: %d" % (min, val))
        if max != None:
            if val > max:
                fail("expecting an integer smaller or equal to %d, got: %d" % (max, val))
        return val

    return check

schema_int = schema(schema_int_ctor)

###############

def schema_action_ctor():
    def check(val):
        assert_type(val, "action")
        return val

    return check

schema_action = schema(schema_action_ctor)

###############

def schema_list_ctor(item=as_is):
    assert_type_schema(item)

    def check(val):
        assert_type(val, "list")
        return [
            item(i)
            for i in val
        ]

    return check

schema_list = schema(schema_list_ctor)

###############

def schema_dict_ctor(key=as_is, value=as_is):
    assert_type_schema(key)
    assert_type_schema(value)

    def check(val):
        assert_type(val, "dict")
        return {
            key(k): value(v)
            for k, v in val.items()
        }

    return check

schema_dict = schema(schema_dict_ctor)

###############

def schema_all_of_ctor(*args):
    assert_list_of(list(args), assert_type_schema)

    def check(val):
        for s in args:
            val = s(val)
        return val

    return check

schema_all_of = schema(schema_all_of_ctor)

###############

def schema_future_getter_ctor():
    def check(val):
        assert_type(val, "future_getter")
        return val

    return check

schema_future_getter = schema(schema_future_getter_ctor)

###############

def schema_future_setter_ctor():
    def check(val):
        assert_type(val, "future_setter")
        return val

    return check

schema_future_setter = schema(schema_future_setter_ctor)

###############

def schema_provider_ctor(ctor):
    assert_type_provider_ctor(ctor)

    def check(val):
        assert_type(val, "provider")
        if val.ctor != ctor:
            fail("%s is not an instance of expected provider" % val)
        return val

    return check

schema_provider = schema(schema_provider_ctor)

###############

def schema_label_ctor(providers=[]):
    assert_list_of(providers, assert_type_provider_ctor)

    def check(val):
        # check_label() is a predeclared function, available only in this file
        return check_label(val, providers)

    return check

schema_label = schema(schema_label_ctor)
