# job_impl is a simple implementation function
def job_impl(ctx):
    ctx.actions.run_container(
        image=ctx.attrs.image,
        args=getattr(ctx.attrs, "args", []),
    )


# job is a step constructor
job = step(
    implementation=job_impl,
    attrs={
        "image": attr.string(),
        "args": attr.list(
            item=attr.string(),
        ),
    },
)

# job() invocation instantiates a step named "job1".
# job_impl() is called here and it instantiates a run_container() action.
job(
    name="job1",
    image="img1:v3",
    # args is omitted here and it will be set to an empty list in the implementation function
)

# job() invocation instantiates a step named "job2".
# job_impl() is called here and it instantiates a run_container() action.
job(
    name="job2",
    image="img1:v3",
    deps=[
        ":job1",
    ],
)
