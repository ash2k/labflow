load("lib_publish_image.star", "build_and_publish_image")
load("lib_scan_image.star", "scan")

build_and_publish_image(
    name = "push_image1",
    image = "img1",
)

#build_and_publish_image(
#    name = "push_image2",
#    image = "img2",
#)

#build_and_publish_image(
#    name = "push_image3",
#    image = "img3",
#)

scan(
    name = "scan_job",
    to_scan = [
        ":push_image1",
        #":push_image2",
        #":push_image3",
    ],
    parallel = False,
)
