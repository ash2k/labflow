load("lib_scan_image.star", "ImageList", "Image")

def build_and_publish_image_impl(sctx):
    image_set = sctx.future()
    public_image_action = sctx.actions.run_container(
        image = sctx.attrs.image,
        args =  getattr(sctx.attrs,"args", []),
        env_vars = {
          "WRITE_IMAGE_NAME_TO": image_set(),
        },
    )
    image_get = image_set(public_image_action)
    return [
        ImageList(
            images = [
                Image(
                    image = image_get,
                ),
            ],
        ),
    ]

build_and_publish_image = step(
    implementation = build_and_publish_image_impl,
    attrs = {
        "image": attr.string(),
        "args": attr.list(
            item = attr.string(),
        ),
    },
)
