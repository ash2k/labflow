scan_image = "scan:v1.0"

Image = provider(
    attrs = {
        "image": attr.future_getter(),
    },
)

ImageList = provider(
    attrs = {
        "images": attr.list(
            item = attr.provider(
                ctor = Image,
            ),
        ),
    },
)

def scan_impl(sctx):
    deps = []
    for to_scan in sctx.attrs.to_scan:
        for image in to_scan[ImageList].images:
            scan_action = sctx.actions.run_container(
                image = scan_image,
                args = [image.image()],
                deps = deps,
                futures = [image.image],
            )
            if not sctx.attrs.parallel:
                deps = [scan_action]

scan = step(
    implementation = scan_impl,
    attrs = {
        "to_scan": attr.list(
            item = attr.label(
                providers = [ImageList],
            ),
        ),
        "parallel": attr.bool(),
    },
)
