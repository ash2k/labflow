load("lib_job.star", "job")

job(
    name = "job1",
    image = "img1:v1",
    args = [
        "-v", "-x",
    ]
)

job(
    name = "job2",
    image = "img2:v3",
    deps = [
        ":job1"
    ],
)
