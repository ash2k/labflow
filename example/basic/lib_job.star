def job_impl(sctx):
    sctx.actions.run_container(
        image = sctx.attrs.image,
        args =  getattr(sctx.attrs,"args", []),
    )

job = step(
    implementation = job_impl,
    attrs = {
        "image": attr.string(),
        "args": attr.list(
            item = attr.string(),
        ),
    },
)
