Message = provider(
    attrs = {
        "message": attr.string(),
    },
)

def produce_greeting_impl(ctx):
    return [
        Message(message="Hello, %s!" % ctx.attrs.person),
        # returning another Message here would cause an error
    ]

produce_greeting = step(
    implementation = produce_greeting_impl,
    attrs = {
        "person": attr.string(),
    },
)

produce_greeting(
    name = "greeting1",
    person = "Sid",
)
def print_message_impl(ctx):
    print(ctx.attrs.to_print[Message].message)

print_message = step(
    implementation = print_message_impl,
    attrs = {
        "to_print": attr.label(
            providers = [
                Message,
            ],
        ),
    },
)

print_message(
    name = "print1",
    to_print = ":greeting1",
)