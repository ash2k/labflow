Message = provider(
    attrs={
        "message": attr.future_getter(),  # future getter for reading.
    },
)


def produce_greeting_impl(ctx):
    msg_set = ctx.future()  # instantiate a new future setter.
    msg_action = ctx.actions.run_container(
        image="greeter:v1",
        args=[
            "--output", msg_set(),  # where to write the greeting
            "--person", ctx.attrs.person,  # Who to greet
        ],
    )
    msg_get = msg_set(msg_action)  # future getter
    return [
        Message(message=msg_get),
    ]


produce_greeting = step(
    implementation=produce_greeting_impl,
    attrs={
        "person": attr.string(),
    },
)

produce_greeting(
    name="greeting1",
    person="Sid",
)


def print_message_impl(ctx):
    message = ctx.attrs.to_print[Message].message
    ctx.actions.run_container(
        image="printer:v1",
        args=[
            # Where to read the greeting from.
            # Note how message is used as a function to get the file name.
            "--input", message(),
        ],
        futures=[message],  # declare dependency from this action onto the producer action via the future.
    )
    # An alternative implementation where contents of the input file are piped into stdin.
    # Note how message is used as a value rather than a function.
    # ctx.actions.run_container(
    #     image="printer:v1",
    #     stdin=message,
    # )


print_message = step(
    implementation=print_message_impl,
    attrs={
        "to_print": attr.label(
            providers=[  # list of must-be-supplied-by-dependency provider types
                Message,
            ],
        ),
    },
)

print_message(
    name="print1",
    to_print=":greeting1",
)