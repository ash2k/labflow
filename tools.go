package labflow

import (
	"errors"
	"fmt"
	"unsafe"

	"go.starlark.net/starlark"
)

func joinStringDicts(dicts ...starlark.StringDict) (starlark.StringDict, error) {
	result := starlark.StringDict{}
	for _, d := range dicts {
		for k, v := range d {
			if _, ok := result[k]; ok {
				return nil, fmt.Errorf("duplicate key: %s", k)
			}
			result[k] = v
		}
	}
	return result, nil
}

func kwargsToSchemas(kwargs []starlark.Tuple, reservedAttributes map[string]schema) (map[string]schema, error) {
	result := make(map[string]schema, len(kwargs)+len(reservedAttributes))
	for _, kv := range kwargs {
		k, ok := kv[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("unexpected key type: expecting string, got: %s", kv[0].Type())
		}
		kStr := string(k)
		if _, ok = reservedAttributes[kStr]; ok {
			return nil, fmt.Errorf("reserved attribute name: %s", kStr)
		}
		v, ok := kv[1].(schema)
		if !ok {
			return nil, fmt.Errorf("unexpected value type: expecting schema, got: %s", kv[1].Type())
		}
		result[kStr] = v
	}
	for name, s := range reservedAttributes {
		result[name] = s
	}
	return result, nil
}

func validateAndDefaultKwargs(thread *starlark.Thread, kwargs []starlark.Tuple, schemas map[string]schema) ([]starlark.Tuple, error) {
	// TODO validate schemas, ensure all required args have been supplied
	// TODO support defaulting/optional arguments
	// TODO check that required reserved attributed have been passed and match their schemas
	// TODO support restricting allowed schema types e.g. provider() must error out on attr.label().
	result := make([]starlark.Tuple, 0, len(kwargs))
	for _, kwarg := range kwargs {
		tup, err := validateAndDefaultKwarg(thread, kwarg, schemas)
		if err != nil {
			return nil, fmt.Errorf("attribute %s: %w", string(kwarg[0].(starlark.String)), err)
		}
		result = append(result, tup)
	}
	return result, nil
}

func validateAndDefaultKwarg(thread *starlark.Thread, kwarg starlark.Tuple, schemas map[string]schema) (starlark.Tuple, error) {
	attrName := string(kwarg[0].(starlark.String))
	s, ok := schemas[attrName]
	if !ok {
		return nil, errors.New("unknown attribute")
	}
	value, err := starlark.Call(thread, s, starlark.Tuple{kwarg[1]}, nil)
	if err != nil {
		return nil, fmt.Errorf("attribute has invalid value: %w", err)
	}
	return starlark.Tuple{kwarg[0], value}, nil
}

// ptrToHash returns an identity hash for pointers.
func ptrToHash(ptr uintptr) uint32 {
	if unsafe.Sizeof(ptr) == 8 { // 64 bit machine
		return uint32(uint64(ptr)>>32) ^ uint32(ptr)
	} else {
		return uint32(ptr)
	}
}
