package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
)

const (
	schemaCtorName = "schema_ctor"
	schemaCtorType = "schema_ctor"
	schemaBuiltin  = "schema"
)

var (
	_ starlark.Callable = schemaCtor{}
)

type schemaCtor struct {
	ctor starlark.Callable
}

func newSchemaCtor(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var ctor starlark.Callable
	err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 1, &ctor)
	if err != nil {
		return nil, err
	}
	return schemaCtor{
		ctor: ctor,
	}, nil
}

func (s schemaCtor) String() string {
	return fmt.Sprintf("<%s>", s.Name())
}

func (s schemaCtor) Type() string {
	return schemaCtorType
}

func (s schemaCtor) Freeze() {
}

func (s schemaCtor) Truth() starlark.Bool {
	return true
}

func (s schemaCtor) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", s.Type())
}

func (s schemaCtor) Name() string {
	return schemaCtorName
}

func (s schemaCtor) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	checkVal, err := starlark.Call(thread, s.ctor, args, kwargs)
	if err != nil {
		return nil, err
	}
	check, ok := checkVal.(starlark.Callable)
	if !ok {
		return nil, fmt.Errorf("expecting a callable, got: %s", checkVal.Type())
	}
	return schema{
		check: check,
	}, nil
}
