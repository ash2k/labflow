package labflow

import (
	"errors"
	"fmt"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"go.starlark.net/syntax"
)

const (
	actionInstanceType = "action"
	actionAttrDeps     = "deps"
	actionAttrFutures  = "futures"
)

type actionsGetter interface {
	AppendActions(*actionsCollector)
}

type actionInstance struct {
	starlarkstruct.Struct
}

func (i *actionInstance) Type() string {
	return actionInstanceType
}

func (i *actionInstance) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", i.Type())
}

func (i *actionInstance) Binary(op syntax.Token, y starlark.Value, side starlark.Side) (starlark.Value, error) {
	return nil, nil // unhandled. Override method on Struct.
}

func (i *actionInstance) CompareSameType(op syntax.Token, y starlark.Value, depth int) (bool, error) {
	return false, fmt.Errorf("%s %s %s not implemented", i.Type(), op, y.Type())
}

func (i *actionInstance) Deps() ([]*actionInstance, error) {
	deps, err := i.Attr(actionAttrDeps)
	if err != nil {
		var notFound starlark.NoSuchAttrError
		if errors.As(err, &notFound) {
			return nil, nil
		}
		return nil, err
	}
	list, ok := deps.(*starlark.List)
	if !ok {
		return nil, fmt.Errorf("expecting a list, got: %s", deps.Type())
	}
	pos := 0
	results := make([]*actionInstance, 0, list.Len())
	var item starlark.Value
	iter := list.Iterate()
	defer iter.Done()
	for iter.Next(&item) {
		a, ok := item.(*actionInstance)
		if !ok {
			return nil, fmt.Errorf("expecting a list of actions, got: %s in position %d", item.Type(), pos)
		}
		pos++
		results = append(results, a)
	}
	return results, nil
}

func (i *actionInstance) Futures() ([]*futureGetter, error) {
	futures, err := i.Attr(actionAttrFutures)
	if err != nil {
		var notFound starlark.NoSuchAttrError
		if errors.As(err, &notFound) {
			return nil, nil
		}
		return nil, err
	}
	list, ok := futures.(*starlark.List)
	if !ok {
		return nil, fmt.Errorf("expecting a list, got: %s", futures.Type())
	}
	pos := 0
	results := make([]*futureGetter, 0, list.Len())
	var item starlark.Value
	iter := list.Iterate()
	defer iter.Done()
	for iter.Next(&item) {
		get, ok := item.(*futureGetter)
		if !ok {
			return nil, fmt.Errorf("expecting a list of futures, got: %s in position %d", item.Type(), pos)
		}
		pos++
		results = append(results, get)
	}
	return results, nil
}

type actionsCollector []*actionInstance

func (c *actionsCollector) AppendFromValue(val starlark.Value) {
	// TODO this function is kind of dangerous as it may lead to infinite recursion if there is a loop. Detect that.
	switch v := val.(type) {
	case *actionInstance:
		*c = append(*c, v)
	case *starlark.List:
		c.appendFromIterable(v)
	case *starlark.Dict:
		for _, t := range v.Items() {
			c.AppendFromValue(t[1])
		}
	case actionsGetter:
		v.AppendActions(c)
	}
}

func (c *actionsCollector) appendFromIterable(val starlark.Iterable) {
	iter := val.Iterate()
	defer iter.Done()
	var item starlark.Value
	for iter.Next(&item) {
		c.AppendFromValue(item)
	}
}
