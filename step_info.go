package labflow

import "go.starlark.net/starlark"

type stepInfo struct {
	name label
	// providers is a dict of providers, returned by this step.
	// *providerCtor -> *providerInstance
	providers *starlark.Dict
	// actions is all actions, instantiated by this step.
	actions []*actionInstance
	futures []*futureSetter
	// deps is a list of explicit user-specified dependencies.
	deps []*stepInfo
}
