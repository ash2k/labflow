package labflow

import (
	"errors"
	"fmt"

	"go.starlark.net/starlark"
)

func newSchemaOrCtor() schemaCtor {
	return schemaCtor{
		ctor: starlark.NewBuiltin("any_of_ctor", schemaOrCtor),
	}
}

func schemaOrCtor(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if len(kwargs) > 0 {
		return nil, errors.New("unexpected keyword arguments")
	}
	for pos, arg := range args {
		if _, ok := arg.(schema); !ok {
			return nil, fmt.Errorf("expecting schema, got: %s in argument position %d", arg.Type(), pos)
		}
	}
	return starlark.NewBuiltin("any_of_check", schemaOrCheck(args)), nil
}

func schemaOrCheck(schemas starlark.Tuple) func(*starlark.Thread, *starlark.Builtin, starlark.Tuple, []starlark.Tuple) (starlark.Value, error) {
	return func(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
		var val starlark.Value
		err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 1, &val)
		if err != nil {
			return nil, err
		}
		var errs []error
		for _, arg := range schemas {
			s := arg.(schema)
			newVal, err := starlark.Call(thread, s, starlark.Tuple{val}, nil)
			if err != nil {
				errs = append(errs, err)
				continue
			}
			return newVal, nil
		}
		if len(errs) > 0 {
			return nil, fmt.Errorf("value doesn't match any of the schemas: %s", errs)
		}
		return val, nil
	}
}
