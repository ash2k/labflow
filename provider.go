package labflow

import (
	"fmt"
	"unsafe"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"go.starlark.net/syntax"
)

const (
	providerInstanceType = "provider"
)

var (
	_ starlark.Comparable = (*providerInstance)(nil)
	_ starlark.HasAttrs   = (*providerInstance)(nil)
	_ actionsGetter       = (*providerInstance)(nil)
)

type providerInstance struct {
	starlarkstruct.Struct
	ctor *providerCtor
}

func (i *providerInstance) Type() string {
	return providerInstanceType
}

func (i *providerInstance) Hash() (uint32, error) {
	hash, err := i.Struct.Hash()
	if err != nil {
		return 0, err
	}
	return hash ^ ptrToHash(uintptr(unsafe.Pointer(i.ctor))), nil
}

func (i *providerInstance) Binary(op syntax.Token, y starlark.Value, side starlark.Side) (starlark.Value, error) {
	return nil, nil // unhandled. Override method on Struct.
}

func (i *providerInstance) CompareSameType(op syntax.Token, yy starlark.Value, depth int) (bool, error) {
	y := yy.(*providerInstance)
	switch op {
	case syntax.EQL:
		if i.ctor != y.ctor {
			return false, nil
		}
		return i.Struct.CompareSameType(syntax.EQL, &y.Struct, depth)
	case syntax.NEQ:
		eq, err := i.CompareSameType(syntax.EQL, yy, depth)
		return !eq, err
	default:
		return false, fmt.Errorf("%s %s %s not implemented", i.Type(), op, y.Type())
	}
}

func (i *providerInstance) AppendActions(collector *actionsCollector) {
	entries := starlark.StringDict{}
	i.Struct.ToStringDict(entries)
	for _, val := range entries {
		collector.AppendFromValue(val)
	}
}
