package labflow

import (
	"fmt"
	"regexp"

	"go.starlark.net/starlark"
)

var (
	labelRegex = regexp.MustCompile("^:([a-z\\d_-]+)$")
)

type label struct {
	Name string
}

func (l label) String() string {
	return ":" + l.Name
}

func parseLabel(lbl starlark.String) (label, error) {
	parts := labelRegex.FindStringSubmatch(string(lbl))
	if parts == nil {
		return label{}, fmt.Errorf("invalid label: %s", string(lbl))
	}
	return label{
		Name: parts[1],
	}, nil
}

type orderedLabels []label

func (p orderedLabels) Len() int           { return len(p) }
func (p orderedLabels) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p orderedLabels) Less(i, j int) bool { return p[i].Name < p[j].Name }
