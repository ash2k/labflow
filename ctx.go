package labflow

import (
	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

type ctx struct {
	starlarkstruct.Module
	stepName   label
	futurePath string
	// Action holds the list of actions that have been created using this ctx.
	actions []*actionInstance
	futures []*futureSetter
}

func newCtx(stepName label, futurePath string, attrs *starlarkstruct.Struct, actionCtors starlark.StringDict) *ctx {
	s := &ctx{
		stepName:   stepName,
		futurePath: futurePath,
	}
	s.Module = starlarkstruct.Module{
		Name: "ctx",
		Members: starlark.StringDict{
			"attrs":   attrs,
			"actions": s.newCtxActions(actionCtors),
			"future":  starlark.NewBuiltin("future", s.newCtxFuture),
		},
	}
	return s
}

func (c *ctx) registerAction(action *actionInstance) {
	c.actions = append(c.actions, action)
}

func (c *ctx) validateAction(action *actionInstance) bool {
	for _, a := range c.actions {
		if a == action {
			return true
		}
	}
	return false
}
