package labflow

import (
	"strconv"

	"go.starlark.net/starlark"
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
)

var (
	_ graph.Node               = (*ActionNode)(nil)
	_ encoding.AttributeSetter = (*ActionNode)(nil)
	_ encoding.Attributer      = (*ActionNode)(nil)
)

type ActionNode struct {
	graph.Node
	action starlark.Value
	attrs  map[string]string
}

func (n *ActionNode) Action() starlark.Value {
	return n.action
}

// Attributes returns the DOT attributes of the node.
func (n *ActionNode) Attributes() []encoding.Attribute {
	res := make([]encoding.Attribute, 0, len(n.attrs)+1)
	if _, ok := n.attrs["label"]; !ok {
		res = append(res, encoding.Attribute{
			Key:   "label",
			Value: strconv.Quote(n.action.String()),
		})
	}
	for k, v := range n.attrs {
		res = append(res, encoding.Attribute{
			Key:   k,
			Value: v,
		})
	}
	return res
}

// SetAttribute sets a DOT attribute.
func (n *ActionNode) SetAttribute(attr encoding.Attribute) error {
	n.attrs[attr.Key] = attr.Value
	return nil
}
