package labflow

import (
	_ "embed"
	"fmt"
	"sort"
	"strings"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"gonum.org/v1/gonum/graph"
)

const (
	defaultInfoActionsAttrName  starlark.String = "actions"
	PredeclaredIdentifiersLocal                 = "predeclaredLabFlowIdentifiers"
	defaultInfoProviderName                     = "DefaultInfo"
	schemaPrefix                                = "schema_"

	predeclaredGlobalStepAttributes   = "step_attributes"
	predeclaredGlobalActionAttributes = "action_attributes"
)

var (
	//go:embed predeclared1_schemas.star
	predeclaredSchemas []byte

	//go:embed predeclared2_globals.star
	predeclaredGlobals []byte

	//go:embed predeclared3_actions.star
	predeclaredActions []byte
)

// Universe contains global state of LabFlow program evaluation.
type Universe struct {
	futurePath             string
	steps                  map[label]*stepInfoHolder
	reservedStepAttributes map[string]schema
	attr                   *starlarkstruct.Module
	defaultInfoCtor        *providerCtor
	actionCtors            starlark.StringDict
	universeGlobals        starlark.StringDict
}

func (u *Universe) predeclared() (starlark.StringDict, error) {
	predeclared, err := joinStringDicts(u.universeGlobals, starlark.StringDict{
		"attr":        u.attr,
		schemaBuiltin: starlark.NewBuiltin(schemaBuiltin, newSchemaCtor),
		"provider":    starlark.NewBuiltin("provider", newProviderCtor),
		"step":        starlark.NewBuiltin("step", u.newStep),
	})
	if err != nil {
		return nil, err
	}
	return predeclared, nil
}

func (u *Universe) addStep(name label, computeStepInfo func() (*stepInfo, error)) error {
	if _, ok := u.steps[name]; ok {
		return fmt.Errorf("step with name %q already defined", name)
	}
	sih := &stepInfoHolder{}
	sih.stepInfo, sih.err = computeStepInfo()
	u.steps[name] = sih
	return sih.err // don't wrap to preserve the backtrace
}

func (u *Universe) getStep(name label) (*stepInfo, error) {
	sih, ok := u.steps[name]
	if !ok {
		return nil, fmt.Errorf("undefined step: %s", name)
	}
	return sih.stepInfo, sih.err
}

func (u *Universe) ActionsGraph() (graph.Directed, error) {
	orderedSteps := make(orderedLabels, 0, len(u.steps))
	for lbl := range u.steps {
		orderedSteps = append(orderedSteps, lbl)
	}
	sort.Sort(orderedSteps)
	b := newGraphBuilder("Steps, actions, and futures")
	for _, lbl := range orderedSteps {
		err := b.AddStep(u.steps[lbl].stepInfo)
		if err != nil {
			return nil, err
		}
	}
	return b.graph, nil
}

func UniverseFromFile(thread *starlark.Thread, filename string, opts ...UniverseOption) (*Universe, error) {
	cfg := defaultUniverseConfig()
	applyOptions(opts)
	u, err := newUniverse(cfg)
	if err != nil {
		return nil, err
	}
	predeclared, err := u.predeclared()
	if err != nil {
		return nil, err
	}
	predeclared, err = joinStringDicts(starlark.Universe, predeclared, cfg.extraPredeclared)
	if err != nil {
		return nil, err
	}
	thread.SetLocal(PredeclaredIdentifiersLocal, predeclared)
	_, err = starlark.ExecFile(thread, filename, cfg.programSrc, predeclared)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func newUniverse(cfg universeConfig) (*Universe, error) {
	thread := &starlark.Thread{
		Name: "load predeclared",
	}
	attrInstance, err := constructAttr(thread)
	if err != nil {
		return nil, err
	}
	universeGlobals, reservedStepAttributes, reservedActionAttributes, err := constructGlobals(thread, attrInstance)
	if err != nil {
		return nil, err
	}
	defaultInfoCtor, ok := universeGlobals[defaultInfoProviderName]
	if !ok {
		return nil, fmt.Errorf("%s provider must be predeclared", defaultInfoProviderName)
	}
	actionCtors, err := constructActions(thread, attrInstance, reservedActionAttributes)
	if err != nil {
		return nil, err
	}
	return &Universe{
		futurePath:             cfg.futurePath,
		steps:                  map[label]*stepInfoHolder{},
		reservedStepAttributes: reservedStepAttributes,
		attr:                   attrInstance,
		defaultInfoCtor:        defaultInfoCtor.(*providerCtor),
		actionCtors:            actionCtors,
		universeGlobals:        universeGlobals,
	}, nil
}

func constructAttr(thread *starlark.Thread) (*starlarkstruct.Module, error) {
	schemas, err := starlark.ExecFile(thread, "predeclared1_schemas.star", predeclaredSchemas, starlark.StringDict{
		schemaBuiltin:     starlark.NewBuiltin(schemaBuiltin, newSchemaCtor),
		checkLabelBuiltin: starlark.NewBuiltin(checkLabelBuiltin, schemaCheckLabel),
	})
	if err != nil {
		return nil, err
	}
	schemasMap, err := extractSchemas(schemas)
	if err != nil {
		return nil, err
	}
	return newAttr(schemasMap, map[string]schemaCtor{
		"any_of": newSchemaOrCtor(),
	})
}

func constructGlobals(thread *starlark.Thread, attrInstance *starlarkstruct.Module) (starlark.StringDict, map[string]schema, map[string]schema, error) {
	globals, err := starlark.ExecFile(thread, "predeclared2_globals.star", predeclaredGlobals, starlark.StringDict{
		"attr":     attrInstance,
		"provider": starlark.NewBuiltin("provider", newProviderCtor),
	})
	if err != nil {
		return nil, nil, nil, err
	}
	universeGlobals := starlark.StringDict{}
	var reservedStepAttributes, reservedActionAttributes map[string]schema
	for n, v := range globals {
		switch val := v.(type) {
		case *starlark.Dict:
			switch n {
			case predeclaredGlobalStepAttributes:
				reservedStepAttributes, err = kwargsToSchemas(val.Items(), nil)
				if err != nil {
					return nil, nil, nil, err
				}
				continue // don't add to universeGlobals
			case predeclaredGlobalActionAttributes:
				reservedActionAttributes, err = kwargsToSchemas(val.Items(), nil)
				if err != nil {
					return nil, nil, nil, err
				}
				continue // don't add to universeGlobals
			}
		}
		universeGlobals[n] = v
	}
	return universeGlobals, reservedStepAttributes, reservedActionAttributes, nil
}

func constructActions(thread *starlark.Thread, attrInstance *starlarkstruct.Module, reservedActionAttributes map[string]schema) (starlark.StringDict, error) {
	globals, err := starlark.ExecFile(thread, "predeclared3_actions.star", predeclaredActions, starlark.StringDict{
		"action": starlark.NewBuiltin("action", newActionCtor(reservedActionAttributes)),
		"attr":   attrInstance,
	})
	if err != nil {
		return nil, err
	}
	actionCtors := starlark.StringDict{}
	for n, v := range globals {
		switch val := v.(type) {
		case *actionCtor:
			val.name = n // initialize name
			actionCtors[n] = v
		}
	}
	return actionCtors, nil
}

func extractSchemas(schemas starlark.StringDict) (map[string]schemaCtor, error) {
	res := map[string]schemaCtor{}
	for name, global := range schemas {
		ctor, ok := global.(schemaCtor)
		if !ok {
			continue
		}
		if !strings.HasPrefix(name, schemaPrefix) {
			return nil, fmt.Errorf("expecting schema ctor name to start with %s, got: %s", schemaPrefix, name)
		}
		res[strings.TrimPrefix(name, schemaPrefix)] = ctor
	}
	return res, nil
}

type stepInfoHolder struct {
	stepInfo *stepInfo
	err      error
}
