package labflow

import (
	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

type ctxActions struct {
	starlarkstruct.Module
}

func (c *ctx) newCtxActions(actions starlark.StringDict) *ctxActions {
	wrappedActions := make(starlark.StringDict, len(actions))
	for k, v := range actions {
		wrappedActions[k] = &ctxActionWrapper{
			actionCtor:     v.(*actionCtor),
			registerAction: c.registerAction,
		}
	}
	return &ctxActions{
		Module: starlarkstruct.Module{
			Name:    "ctx.actions",
			Members: wrappedActions,
		},
	}
}

type ctxActionWrapper struct {
	*actionCtor
	registerAction func(*actionInstance)
}

func (w *ctxActionWrapper) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	value, err := w.actionCtor.CallInternal(thread, args, kwargs)
	if err != nil {
		return nil, err
	}
	w.registerAction(value.(*actionInstance))
	return value, nil
}
