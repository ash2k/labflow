package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
)

const (
	futureGetterType = "future_getter"
	futureGetterName = "future_getter"
)

var (
	_ starlark.Value    = (*futureGetter)(nil)
	_ starlark.Callable = (*futureGetter)(nil)
)

type futureGetter struct {
	file string
	set  *futureSetter
	// value holds either a starlark.String or an *actionInstance.
	value starlark.Value
}

func (g *futureGetter) Name() string {
	return futureGetterName
}

func (g *futureGetter) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	return starlark.String(g.file), nil
}

func (g *futureGetter) String() string {
	return fmt.Sprintf("<%s: %s>", g.Name(), g.file)
}

func (g *futureGetter) Type() string {
	return futureGetterType
}

func (g *futureGetter) Freeze() {
	g.value.Freeze()
}

func (g *futureGetter) Truth() starlark.Bool {
	return false
}

func (g *futureGetter) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", g.Type())
}
