package labflow

import (
	"fmt"

	"gonum.org/v1/gonum/graph/encoding"
)

const (
	// EdgeTypeAttributeKey is the key of an edge attribute that is applied to all edges
	// that represent dependencies among actions.
	EdgeTypeAttributeKey = "labflow_edge_type"
	// EdgeTypeAction is the value of the EdgeTypeAttributeKey edge attribute that means the edge was added
	// because of action to action dependency.
	EdgeTypeAction = "action"
	// EdgeTypeDep is the value of the EdgeTypeAttributeKey edge attribute that means the edge was added
	// because of step to step dependency via the "deps" attribute on a step.
	EdgeTypeDep = "dep"
	// EdgeTypeFuture is the value of the EdgeTypeAttributeKey edge attribute that means the edge was added
	// because of a dependency of an action onto a future or vice versa.
	EdgeTypeFuture = "future"
)

type graphBuilder struct {
	graph       *customGraph
	action2node map[*actionInstance]*ActionNode
	future2node map[*futureSetter]*FutureNode
	stepCounter int32
}

func newGraphBuilder(name string) *graphBuilder {
	return &graphBuilder{
		graph:       newCustomGraph(name),
		action2node: map[*actionInstance]*ActionNode{},
		future2node: map[*futureSetter]*FutureNode{},
	}
}

func (b *graphBuilder) AddStep(si *stepInfo) error {
	stepGraph := newCustomGraph(fmt.Sprintf("cluster%d", b.stepCounter))
	b.stepCounter++
	stepGraphAttrs, _, _ := stepGraph.DOTAttributeSetters()
	err := stepGraphAttrs.SetAttribute(encoding.Attribute{
		Key:   "label",
		Value: si.name.String(),
	})
	if err != nil {
		return err
	}
	for _, set := range si.futures {
		err = b.addFutureToActionEdge(stepGraph, set)
		if err != nil {
			return err
		}
	}
	var depsActions actionsCollector
	for _, dep := range si.deps {
		depsActions.AppendFromValue(dep.providers)
	}
	for _, a := range si.actions {
		err = b.addAction(stepGraph, a, depsActions)
		if err != nil {
			return err
		}
	}
	b.graph.sub = append(b.graph.sub, stepGraph)
	return nil
}

func (b *graphBuilder) addFutureToActionEdge(stepGraph *customGraph, set *futureSetter) error {
	fNode := b.getFutureNode(set)
	stepGraph.AddNode(fNode)
	a, ok := set.get.value.(*actionInstance)
	if !ok {
		return nil
	}
	aNode := b.getActionNode(a)
	if b.graph.HasEdgeFromTo(fNode.ID(), aNode.ID()) {
		return fmt.Errorf("unexpceted edge from %d to %d", fNode.ID(), aNode.ID())
	}
	edge := b.graph.NewEdge(fNode, aNode)
	err := edge.(encoding.AttributeSetter).SetAttribute(encoding.Attribute{
		Key:   EdgeTypeAttributeKey,
		Value: EdgeTypeFuture,
	})
	if err != nil {
		return err
	}
	b.graph.SetEdge(edge)
	return nil
}

func (b *graphBuilder) addActionToFutureEdge(aNode *ActionNode, get *futureGetter) error {
	fNode := b.getFutureNode(get.set)
	if b.graph.HasEdgeFromTo(aNode.ID(), fNode.ID()) {
		return fmt.Errorf("unexpceted edge from %d to %d", aNode.ID(), fNode.ID())
	}
	edge := b.graph.NewEdge(aNode, fNode)
	err := edge.(encoding.AttributeSetter).SetAttribute(encoding.Attribute{
		Key:   EdgeTypeAttributeKey,
		Value: EdgeTypeFuture,
	})
	if err != nil {
		return err
	}
	b.graph.SetEdge(edge)
	return nil
}

func (b *graphBuilder) addAction(stepGraph *customGraph, a *actionInstance, deps []*actionInstance) error {
	aNode := b.getActionNode(a)
	stepGraph.AddNode(aNode)

	// 1. Add edges from this action to all futures it directly depends on.
	futures, err := a.Futures()
	if err != nil {
		return err
	}
	for _, get := range futures {
		err = b.addActionToFutureEdge(aNode, get)
		if err != nil {
			return err
		}
	}

	// 2. Add edges from this action to all actions it directly depends on.
	directDeps, err := a.Deps()
	if err != nil {
		return err
	}
	err = b.addActionEdges(aNode, directDeps, []encoding.Attribute{
		{
			Key:   EdgeTypeAttributeKey,
			Value: EdgeTypeAction,
		},
	})
	if err != nil {
		return err
	}

	// 3. Add edges from this action to all actions of the step's dependencies
	err = b.addActionEdges(aNode, deps, []encoding.Attribute{
		{
			Key:   EdgeTypeAttributeKey,
			Value: EdgeTypeDep,
		},
	})
	if err != nil {
		return err
	}
	return nil
}

// addActionEdges adds missing edges from aID to deps using attrs attributes for the edges.
func (b *graphBuilder) addActionEdges(aNode *ActionNode, deps []*actionInstance, attrs []encoding.Attribute) error {
	for _, depAction := range deps {
		bNode := b.getActionNode(depAction)
		if b.graph.HasEdgeFromTo(aNode.ID(), bNode.ID()) {
			// There is an edge already, don't overwrite it to preserve original attributes.
			return nil
		}
		edge := b.graph.NewEdge(aNode, bNode)
		err := setAttributes(edge.(encoding.AttributeSetter), attrs...)
		if err != nil {
			return err
		}
		b.graph.SetEdge(edge)
	}
	return nil
}

func (b *graphBuilder) getActionNode(a *actionInstance) *ActionNode {
	node, ok := b.action2node[a]
	if !ok {
		node = &ActionNode{
			Node:   b.graph.NewNode(),
			action: a,
			attrs:  map[string]string{},
		}
		b.graph.AddNode(node)
		b.action2node[a] = node
	}
	return node
}

func (b *graphBuilder) getFutureNode(set *futureSetter) *FutureNode {
	node, ok := b.future2node[set]
	if !ok {
		node = &FutureNode{
			Node:   b.graph.NewNode(),
			future: set,
			attrs:  map[string]string{},
		}
		b.graph.AddNode(node)
		b.future2node[set] = node
	}
	return node
}

func setAttributes(setter encoding.AttributeSetter, attrs ...encoding.Attribute) error {
	for _, attr := range attrs {
		err := setter.SetAttribute(attr)
		if err != nil {
			return err
		}
	}
	return nil
}
