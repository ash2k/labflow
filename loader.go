package labflow

import (
	"fmt"
	"path/filepath"

	"go.starlark.net/starlark"
)

type loaderEntry struct {
	globals starlark.StringDict
	err     error
}

type loader struct {
	cache   map[string]*loaderEntry
	rootDir string
}

func (l *loader) Load(thread *starlark.Thread, module string) (starlark.StringDict, error) {
	e, ok := l.cache[module]
	if e == nil {
		if ok {
			// request for package whose loading is in progress
			return nil, fmt.Errorf("cycle in load() graph")
		}

		// Add a placeholder to indicate "load in progress".
		l.cache[module] = nil

		// Load the module.
		loadThread := &starlark.Thread{
			Name:  "exec " + module,
			Print: thread.Print,
			Load:  thread.Load,
		}
		predeclared := thread.Local(PredeclaredIdentifiersLocal).(starlark.StringDict)
		loadThread.SetLocal(PredeclaredIdentifiersLocal, predeclared)
		modulePath := module
		if !filepath.IsAbs(module) {
			modulePath = filepath.Join(l.rootDir, module)
		}
		globals, err := starlark.ExecFile(loadThread, modulePath, nil, predeclared)
		e = &loaderEntry{
			globals: globals,
			err:     err,
		}

		// Store in the cache.
		l.cache[module] = e
	}
	return e.globals, e.err
}

func Loader(rootDir string) func(thread *starlark.Thread, module string) (starlark.StringDict, error) {
	l := loader{
		cache:   map[string]*loaderEntry{},
		rootDir: rootDir,
	}
	return l.Load
}
