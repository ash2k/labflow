package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

func newAttr(schemas ...map[string]schemaCtor) (*starlarkstruct.Module, error) {
	members := starlark.StringDict{}
	for _, s := range schemas {
		for name, sch := range s {
			if _, ok := members[name]; ok {
				return nil, fmt.Errorf("duplicate schema: %s", name)
			}
			members[name] = sch
		}
	}
	members.Freeze()
	return &starlarkstruct.Module{
		Name:    "attr",
		Members: members,
	}, nil
}
