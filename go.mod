module gitlab.com/ash2k/labflow

go 1.20

require (
	github.com/google/go-cmp v0.5.9
	github.com/stretchr/testify v1.8.4
	go.starlark.net v0.0.0-20230525235612-a134d8f9ddca
	gonum.org/v1/gonum v0.13.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
