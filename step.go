package labflow

import (
	"errors"
	"fmt"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

const (
	stepAttrName = "name"
	stepAttrDeps = "deps"
)

var (
	_ starlark.Callable = (*step)(nil)
)

type step struct {
	futurePath          string
	implementation      starlark.Callable
	attrSchemas         map[string]schema
	addStepToUniverse   func(label, func() (*stepInfo, error)) error
	getStepFromUniverse func(label) (*stepInfo, error)
	defaultInfoCtor     *providerCtor
	actionCtors         starlark.StringDict
}

func (u *Universe) newStep(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	fnname := fn.Name()
	if args.Len() > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", fnname)
	}
	var (
		implementation starlark.Callable
		attrs          *starlark.Dict
	)
	err := starlark.UnpackArgs(fnname, args, kwargs, "implementation", &implementation, "attrs??", &attrs)
	if err != nil {
		return nil, err
	}
	var attrSchemas map[string]schema
	if attrs != nil {
		attrSchemas, err = kwargsToSchemas(attrs.Items(), u.reservedStepAttributes)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", fnname, err)
		}
	}
	return &step{
		futurePath:          u.futurePath,
		implementation:      implementation,
		attrSchemas:         attrSchemas,
		addStepToUniverse:   u.addStep,
		getStepFromUniverse: u.getStep,
		defaultInfoCtor:     u.defaultInfoCtor,
		actionCtors:         u.actionCtors,
	}, nil
}

func (s *step) String() string {
	return fmt.Sprintf("<step implementation=%q>", s.implementation)
}

func (s *step) Type() string {
	return "step"
}

func (s *step) Freeze() {
	s.implementation.Freeze()
	for _, v := range s.attrSchemas {
		v.Freeze()
	}
}

func (s *step) Truth() starlark.Bool {
	return true
}

func (s *step) Hash() (uint32, error) {
	return 0, fmt.Errorf("unhashable: %s", s.Type())
}

func (s *step) Name() string {
	return "step_ctor"
}

func (s *step) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if args.Len() > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", s.Name())
	}
	kwargs, err := validateAndDefaultKwargs(thread, kwargs, s.attrSchemas)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", s.Name(), err)
	}
	depsList, err := s.getStepDepsAttrValue(kwargs)
	if err != nil {
		return nil, fmt.Errorf("%s: %s attribute: %w", s.Name(), stepAttrDeps, err)
	}
	kwargs, err = s.preprocessLabelsInKwargs(kwargs)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", s.Name(), err)
	}
	attrs := starlarkstruct.FromKeywords(starlarkstruct.Default, kwargs)
	name, err := getStepNameAttrValue(attrs)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", s.Name(), err)
	}
	err = s.addStepToUniverse(name, func() (*stepInfo, error) {
		providers, actions, futures, err := s.callImplementation(name, thread, attrs)
		if err != nil {
			return nil, err
		}
		return &stepInfo{
			name:      name,
			providers: providers,
			actions:   actions,
			futures:   futures,
			deps:      depsList,
		}, nil
	})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", s.Name(), err)
	}
	return starlark.None, nil
}

// preprocessLabelsInKwargs turns labels into corresponding dicts of providers.
func (s *step) preprocessLabelsInKwargs(kwargs []starlark.Tuple) ([]starlark.Tuple, error) {
	result := make([]starlark.Tuple, 0, len(kwargs))
	for _, kwarg := range kwargs {
		tup, err := s.preprocessLabelsInKwarg(kwarg)
		if err != nil {
			return nil, fmt.Errorf("attribute %s: %w", string(kwarg[0].(starlark.String)), err)
		}
		result = append(result, tup)
	}
	return result, nil
}

func (s *step) preprocessLabelsInKwarg(kwarg starlark.Tuple) (starlark.Tuple, error) {
	newVal, err := s.preprocessLabelsInValue(kwarg[1])
	if err != nil {
		return nil, err
	}
	return starlark.Tuple{kwarg[0], newVal}, nil
}

func (s *step) preprocessLabelsInValue(val starlark.Value) (starlark.Value, error) {
	// TODO detect cycles
	switch v := val.(type) {
	case labelAndProviders:
		si, err := s.getStepFromUniverse(v.Label)
		if err != nil {
			return nil, err
		}
		err = validateRequiredProviders(v, si)
		if err != nil {
			return nil, err
		}
		return si.providers, nil
	case *starlark.List:
		newList := make([]starlark.Value, 0, v.Len())
		iter := v.Iterate()
		defer iter.Done()
		var item starlark.Value
		for iter.Next(&item) {
			newItem, err := s.preprocessLabelsInValue(item)
			if err != nil {
				return nil, err
			}
			newList = append(newList, newItem)
		}
		return starlark.NewList(newList), nil
	case *starlark.Dict:
		newDict := starlark.NewDict(v.Len())
		for _, kv := range v.Items() {
			newV, err := s.preprocessLabelsInValue(kv[1])
			if err != nil {
				return nil, err
			}
			err = newDict.SetKey(kv[0], newV)
			if err != nil {
				return nil, err
			}
		}
		return newDict, nil
	case starlark.Tuple:
		newTuple := make(starlark.Tuple, 0, v.Len())
		for _, tv := range v {
			newTV, err := s.preprocessLabelsInValue(tv)
			if err != nil {
				return nil, err
			}
			newTuple = append(newTuple, newTV)
		}
		return newTuple, nil
	case *starlark.Set: // sets are disabled by default but we still handle them here for completeness.
		newSet := starlark.NewSet(v.Len())
		iter := v.Iterate()
		defer iter.Done()
		var item starlark.Value
		for iter.Next(&item) {
			newItem, err := s.preprocessLabelsInValue(item)
			if err != nil {
				return nil, err
			}
			err = newSet.Insert(newItem)
			if err != nil {
				return nil, err
			}
		}
		return newSet, nil
	default:
		return val, nil
	}
}

func (s *step) callImplementation(name label, thread *starlark.Thread, attrs *starlarkstruct.Struct) (*starlark.Dict, []*actionInstance, []*futureSetter, error) {
	// Prepare ctx argument
	ctxArg := newCtx(name, s.futurePath, attrs, s.actionCtors)
	args := starlark.Tuple{ctxArg}
	args.Freeze()
	// Call implementation function
	result, err := starlark.Call(thread, s.implementation, args, nil)
	if err != nil {
		return nil, nil, nil, err
	}
	providers, err := s.extractProviders(thread, result, ctxArg.actions)
	if err != nil {
		return nil, nil, nil, err
	}
	for _, setter := range ctxArg.futures {
		if setter.get == nil {
			return nil, nil, nil, errors.New("not all futures have been set to a value")
		}
	}
	for _, elem := range ctxArg.actions {
		elem.Freeze()
	}
	return providers, ctxArg.actions, ctxArg.futures, nil
}

func (s *step) extractProviders(thread *starlark.Thread, result starlark.Value, actions []*actionInstance) (*starlark.Dict, error) {
	// Check returned value
	var providers *starlark.Dict
	switch res := result.(type) {
	case *starlark.List:
		var err error
		providers, err = listToProviderDict(res)
		if err != nil {
			return nil, err
		}
	case starlark.NoneType:
		// Construct empty dict for DefaultInfo added below
		providers = starlark.NewDict(1)
	default:
		return nil, fmt.Errorf("expecting list or none type but implementation returned: %s", result.Type())
	}
	// Ensure DefaultInfo is present in the providers dict, add one if missing
	_, found, err := providers.Get(s.defaultInfoCtor)
	if err != nil {
		return nil, err
	}
	if !found {
		defaultInfoInst, err := starlark.Call(thread, s.defaultInfoCtor, nil, []starlark.Tuple{
			{
				defaultInfoActionsAttrName, actionsToList(actions),
			},
		})
		if err != nil {
			return nil, err
		}
		err = providers.SetKey(s.defaultInfoCtor, defaultInfoInst)
		if err != nil {
			return nil, err
		}
	}
	providers.Freeze()
	return providers, nil
}

func (s *step) getStepDepsAttrValue(kwargs []starlark.Tuple) ([]*stepInfo, error) {
	for _, kwarg := range kwargs {
		name := string(kwarg[0].(starlark.String))
		if name != stepAttrDeps {
			continue
		}
		list, ok := kwarg[1].(*starlark.List)
		if !ok {
			return nil, fmt.Errorf("expecting list, got: %s", kwarg[1].Type())
		}
		result := make([]*stepInfo, 0, list.Len())
		iter := list.Iterate()
		defer iter.Done()
		var item starlark.Value
		for iter.Next(&item) {
			lbl, ok := item.(labelAndProviders)
			if !ok {
				return nil, fmt.Errorf("expecting string, got: %s", item.Type())
			}
			si, err := s.getStepFromUniverse(lbl.Label)
			if err != nil {
				return nil, err
			}
			result = append(result, si)
		}
		return result, nil
	}
	return nil, nil
}

func validateRequiredProviders(v labelAndProviders, si *stepInfo) error {
	for _, ctor := range v.RequiredProviders {
		_, found, err := si.providers.Get(ctor)
		if err != nil {
			return err
		}
		if !found {
			// TODO this will not print the provider's name
			return fmt.Errorf("%s does not supply required provider %s", v, ctor)
		}
	}
	return nil
}

func actionsToList(actions []*actionInstance) *starlark.List {
	vals := make([]starlark.Value, 0, len(actions))
	for _, a := range actions {
		vals = append(vals, a)
	}
	return starlark.NewList(vals)
}

func listToProviderDict(list *starlark.List) (*starlark.Dict, error) {
	result := starlark.NewDict(list.Len())
	iter := list.Iterate()
	defer iter.Done()
	var item starlark.Value
	for iter.Next(&item) {
		prov, ok := item.(*providerInstance)
		if !ok {
			return nil, fmt.Errorf("expecting a provider from implementation func, got: %s", item.Type())
		}
		_, found, err := result.Get(prov.ctor)
		if err != nil {
			return nil, err
		}
		if found {
			return nil, fmt.Errorf("duplicate provider of type: %s", item.Type())
		}
		err = result.SetKey(prov.ctor, prov)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

func getStepNameAttrValue(attrs *starlarkstruct.Struct) (label, error) {
	nameAttr, err := attrs.Attr(stepAttrName)
	if err != nil {
		var e starlark.NoSuchAttrError
		if errors.As(err, &e) {
			return label{}, fmt.Errorf("%s attribute not specified", stepAttrName)
		}
		return label{}, err
	}
	name, ok := nameAttr.(starlark.String)
	if !ok {
		return label{}, fmt.Errorf("%s attribute: expecting string, got: %s", stepAttrName, nameAttr.Type())
	}
	parsedName, err := parseLabel(":" + name)
	if err != nil {
		return label{}, fmt.Errorf("%s attribute: %w", stepAttrName, err)
	}
	return parsedName, nil
}
