package labflow

import (
	"fmt"

	"go.starlark.net/starlark"
)

const (
	checkLabelBuiltin     = "check_label"
	labelAndProvidersType = "label"
)

var (
	_ starlark.Value = labelAndProviders{}
)

type labelAndProviders struct {
	Label             label
	RequiredProviders []*providerCtor
}

func (l labelAndProviders) Type() string {
	return labelAndProvidersType
}

func (l labelAndProviders) Freeze() {
}

func (l labelAndProviders) Truth() starlark.Bool {
	return true
}

func (l labelAndProviders) Hash() (uint32, error) {
	// Don't care about RequiredProviders here.
	hash, err := starlark.String(l.Label.Name).Hash()
	if err != nil {
		return 0, err
	}
	return 16777619 * hash, nil
}

func (l labelAndProviders) String() string {
	return l.Label.String()
}

func schemaCheckLabel(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var (
		lblStr    starlark.String
		providers *starlark.List
	)
	err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 2, &lblStr, &providers)
	if err != nil {
		return nil, err
	}
	pos := 0
	requiredProviders := make([]*providerCtor, 0, providers.Len())
	var item starlark.Value
	iter := providers.Iterate()
	defer iter.Done()
	for iter.Next(&item) {
		ctor, ok := item.(*providerCtor)
		if !ok {
			// This shouldn't happen because we have validated it in Starlark code, but we handle it here too
			// as a defence in depth against bugs.
			return nil, fmt.Errorf("expecting a list of providers, got: %s in position %d", item.Type(), pos)
		}
		pos++
		requiredProviders = append(requiredProviders, ctor)
	}

	lblParsed, err := parseLabel(lblStr)
	if err != nil {
		return nil, err
	}
	return labelAndProviders{
		Label:             lblParsed,
		RequiredProviders: requiredProviders,
	}, nil
}
