package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/ash2k/labflow"
	"go.starlark.net/lib/json"
	"go.starlark.net/lib/math"
	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/topo"
)

const (
	colorScheme      = "paired12"
	edgeTypeDepColor = "12"

	edgeTypeFutureColor = "4"
	nodeFutureShape     = "diamond"
	nodeFutureFillColor = "3"

	edgeTypeActionColor = "2"
	nodeActionShape     = "ellipse"
	nodeActionFillColor = "1"
)

func main() {
	n := len(os.Args)
	if n < 2 || n > 3 {
		log.Fatalf("%s usage: <workflow file> [workflow name]", filepath.Base(os.Args[0]))
		return
	}
	err := doMain(os.Args[1])
	if err != nil {
		evalErr := getDeepestEvalError(err)
		if evalErr != nil {
			log.Fatalf("Error: %v\n%s", err, evalErr.Backtrace())
		} else {
			log.Fatalf("Error: %v", err)
		}
		return
	}
}

func doMain(filename string) error {
	thread := &starlark.Thread{
		Name: "my thread",
		Load: labflow.Loader(filepath.Dir(filename)),
	}
	u, err := labflow.UniverseFromFile(thread, filename,
		labflow.WithExtraPredeclared(newPredeclared()),
	)
	if err != nil {
		return fmt.Errorf("UniverseFromFile(%s): %w", filename, err)
	}
	g, err := u.ActionsGraph()
	if err != nil {
		return fmt.Errorf("ActionsGraph: %w", err)
	}
	err = printGraph(g, os.Stderr)
	if err != nil {
		return fmt.Errorf("printGraph: %w", err)
	}
	err = dotForGraph(g, filename, os.Stdout)
	if err != nil {
		return fmt.Errorf("dotForGraph: %w", err)
	}
	return nil
}

func printGraph(g graph.Directed, w io.Writer) error {
	nodes, err := topo.Sort(g)
	if err != nil {
		return fmt.Errorf("Sort(): %w", err)
	}
	for _, node := range nodes {
		var s interface{}
		switch n := node.(type) {
		case *labflow.ActionNode:
			s = n.Action()
		case *labflow.FutureNode:
			s = n.Future()
		default:
			s = fmt.Sprintf("<unknown node type %T>", node)
		}
		_, err = fmt.Fprintf(w, "%d: %s\n", node.ID(), s)
		if err != nil {
			return err
		}
	}
	for _, node := range nodes {
		outgoingIter := g.From(node.ID())
		for outgoingIter.Next() {
			_, err = fmt.Fprintf(w, "%d depends on %d\n", node.ID(), outgoingIter.Node().ID())
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func dotForGraph(g graph.Directed, name string, w io.Writer) error {
	err := setGraphAttributes(g)
	if err != nil {
		return err
	}
	data, err := dot.Marshal(g, name, "", "")
	if err != nil {
		return fmt.Errorf("dot.Marshal(): %w", err)
	}
	_, err = w.Write(data)
	if err != nil {
		return fmt.Errorf("write(): %w", err)
	}
	return nil
}

func setGraphAttributes(g graph.Directed) error {
	err := setEdgeAttributes(g)
	if err != nil {
		return fmt.Errorf("setEdgeAttributes: %w", err)
	}
	err = setNodeAttributes(g)
	if err != nil {
		return fmt.Errorf("setNodeAttributes: %w", err)
	}
	gr, node, edge := g.(dot.AttributeSetters).DOTAttributeSetters()
	graphAttributes, edgeAttributes, nodeAttributes := attrsForGraph()
	err = setAttributes(gr, graphAttributes...)
	if err != nil {
		return err
	}
	err = setAttributes(node, nodeAttributes...)
	if err != nil {
		return err
	}
	err = setAttributes(edge, edgeAttributes...)
	if err != nil {
		return err
	}
	return nil
}

func setEdgeAttributes(g graph.Directed) error {
	iterA := g.Nodes()
	for iterA.Next() {
		a := iterA.Node()
		iterB := g.From(a.ID())
		for iterB.Next() {
			b := iterB.Node()
			edge := g.Edge(a.ID(), b.ID())
			for _, attr := range edge.(encoding.Attributer).Attributes() {
				if attr.Key != labflow.EdgeTypeAttributeKey {
					continue // check next attribute
				}
				var color string
				switch attr.Value {
				case labflow.EdgeTypeAction:
					color = edgeTypeActionColor
				case labflow.EdgeTypeDep:
					color = edgeTypeDepColor
				case labflow.EdgeTypeFuture:
					color = edgeTypeFutureColor
				default:
					return fmt.Errorf("unexpected %s value: %q", labflow.EdgeTypeAttributeKey, attr.Value)
				}
				err := setAttributes(edge.(encoding.AttributeSetter), encoding.Attribute{
					Key:   "color",
					Value: color,
				})
				if err != nil {
					return err
				}
				break // go to next edge
			}
		}
	}
	return nil
}

func setNodeAttributes(g graph.Directed) error {
	type structInterface interface {
		ToStringDict(starlark.StringDict)
		Constructor() starlark.Value
	}
	iter := g.Nodes()
	for iter.Next() {
		switch n := iter.Node().(type) {
		case *labflow.ActionNode:
			str := n.Action().(structInterface)
			actionAttrs := starlark.StringDict{}
			str.ToStringDict(actionAttrs)
			delete(actionAttrs, "deps") // don't want it to clutter the graph. We represent deps as edges already.
			err := setAttributes(n,
				encoding.Attribute{
					Key:   "shape",
					Value: nodeActionShape,
				},
				encoding.Attribute{
					Key:   "fillcolor",
					Value: nodeActionFillColor,
				},
				encoding.Attribute{
					Key:   "label",
					Value: starlarkstruct.FromStringDict(str.Constructor(), actionAttrs).String(),
				},
			)
			if err != nil {
				return err
			}
		case *labflow.FutureNode:
			err := setAttributes(n,
				encoding.Attribute{
					Key:   "shape",
					Value: nodeFutureShape,
				},
				encoding.Attribute{
					Key:   "fillcolor",
					Value: nodeFutureFillColor,
				},
			)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func setAttributes(setter encoding.AttributeSetter, attrs ...encoding.Attribute) error {
	for _, attr := range attrs {
		err := setter.SetAttribute(attr)
		if err != nil {
			return err
		}
	}
	return nil
}

func newPredeclared() starlark.StringDict {
	return starlark.StringDict{
		"json":   json.Module,
		"math":   math.Module,
		"struct": starlark.NewBuiltin("struct", starlarkstruct.Make),
	}
}

// getDeepestEvalError returns the deepest starlark.EvalError in the error cause chain.
// When errors are returned, the Starlark interpreter wraps them in starlark.EvalError if they are of a different
// type. They are often of a different type since we wrap errors with fmt.Errorf() to add context.
// Each re-wrapping in starlark.EvalError adds a current stacktrace to the error, hiding the original one.
// This function finds the deepest, earliest starlark.EvalError to get the original stacktrace.
// We cannot just stop wrapping the original error with fmt.Errorf() because wouldn't have enough debugging context
// in the message in that case.
func getDeepestEvalError(err error) *starlark.EvalError {
	var ret *starlark.EvalError
	for err != nil {
		evalErr, ok := err.(*starlark.EvalError)
		if ok {
			ret = evalErr
		}
		err = errors.Unwrap(err)
	}
	return ret
}

func attrsForGraph() ([]encoding.Attribute, []encoding.Attribute, []encoding.Attribute) {
	// DOT Guide: https://www.graphviz.org/pdf/dotguide.pdf
	// Colors https://www.graphviz.org/doc/info/colors.html

	const (
		clusterFillColor    = "11"
		clusterOutlineColor = edgeTypeDepColor
	)

	colorSchemeAttr := encoding.Attribute{
		Key:   "colorscheme",
		Value: colorScheme,
	}
	graphAttributes := []encoding.Attribute{
		colorSchemeAttr,
		{
			Key:   "color",
			Value: clusterOutlineColor,
		},
		{
			Key:   "style",
			Value: "filled",
		},
		{
			Key:   "fillcolor",
			Value: clusterFillColor,
		},
	}
	edgeAttributes := []encoding.Attribute{
		colorSchemeAttr,
	}
	nodeAttributes := []encoding.Attribute{
		colorSchemeAttr,
		{
			Key:   "style",
			Value: "filled",
		},
	}
	return graphAttributes, edgeAttributes, nodeAttributes
}
