package labflow

import (
	"errors"
	"fmt"
	"unsafe"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"go.starlark.net/syntax"
)

const (
	providerCtorName = "provider_ctor"
	providerCtorType = "provider_ctor"

	providerCtorAttribute starlark.String = "ctor"
)

var (
	_ starlark.Callable   = (*providerCtor)(nil)
	_ starlark.Comparable = (*providerCtor)(nil)
)

type providerCtor struct {
	starlarkstruct.Struct
	attrSchemas map[string]schema // don't need to freeze because same values as in Struct
}

func (c *providerCtor) String() string {
	return fmt.Sprintf("<%s>", c.Name())
}

func (c *providerCtor) Type() string {
	return providerCtorType
}

func (c *providerCtor) Hash() (uint32, error) {
	return ptrToHash(uintptr(unsafe.Pointer(c))), nil
}

func (c *providerCtor) Name() string {
	return providerCtorName
}

func (c *providerCtor) CallInternal(thread *starlark.Thread, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if args.Len() > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", c.Name())
	}
	kwargs, err := validateAndDefaultKwargs(thread, kwargs, c.attrSchemas)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", c.Name(), err)
	}
	kwargs = append(kwargs, starlark.Tuple{providerCtorAttribute, c})
	provider := &providerInstance{
		Struct: *starlarkstruct.FromKeywords(starlark.String(providerInstanceType), kwargs),
		ctor:   c,
	}
	provider.Freeze()
	return provider, nil
}

func (c *providerCtor) CompareSameType(op syntax.Token, yy starlark.Value, depth int) (bool, error) {
	y := yy.(*providerCtor)
	switch op {
	case syntax.EQL:
		return c == y, nil
	case syntax.NEQ:
		return c != y, nil
	default:
		return false, fmt.Errorf("%s %s %s not implemented", c.Type(), op, y.Type())
	}
}

func newProviderCtor(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if args.Len() > 0 {
		return nil, errors.New("provider_ctor: unexpected positional arguments")
	}
	var (
		attrs       *starlark.Dict
		attrsKwargs []starlark.Tuple
		attrSchemas map[string]schema
	)
	err := starlark.UnpackArgs("provider", nil, kwargs, "attrs??", &attrs)
	if err != nil {
		return nil, err
	}
	if attrs != nil {
		_, found, err := attrs.Get(providerCtorAttribute)
		if err != nil {
			return nil, err
		}
		if found {
			return nil, fmt.Errorf("provider: %s is a reserved attribute", string(providerCtorAttribute))
		}
		attrsKwargs = attrs.Items()
		attrSchemas, err = kwargsToSchemas(attrsKwargs, nil)
		if err != nil {
			return nil, fmt.Errorf("provider: %w", err)
		}
	}
	ctor := &providerCtor{
		Struct:      *starlarkstruct.FromKeywords(starlark.String(providerCtorName), attrsKwargs),
		attrSchemas: attrSchemas,
	}
	ctor.Freeze()
	return ctor, nil
}
